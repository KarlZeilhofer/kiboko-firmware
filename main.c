/*
 * main.c
 *
 *  Created on: 07.06.2012
 *      Author: fritz
 */

// this file is a copy of the main.c of the project "test_rf-module_v0.1"


/*
 * Tags in the proejct:
 * 		MCK-TAG: things which depend on the MCK-Frequency
 * 		MAIN-TAG: dinge, die im großen dann berücksichtigt werden müssen
 */
// TODO:
/*
 * Unbenutzte Pins auf Ausgang schalten
 *
 *
 * Am Feld:
 * Sync-Time-Out vergrößern
 * Sendeleistung maximieren
 * Trigger-Sperrzeit erhöhen
 *
 *
 *
 */

#define FW_VERSION_YEAR 17
#define FW_VERSION_RELEASE 0
#define FW_VERSION_DEV 1


#include "rfswshared.h"
#include "defs.h"
#include "settings.h"
#include "timebase.h"
#include "triggerstation.h"
#include "boatbox.h"
#include "matrixboard.h"
#include "dbgupdc.h"
#include "softpll.h"

#include "myalloc.h"

// DEFINITIONS:

	/* PIT_MODE_REG
	 * is needed for the time-synchronization.
	 * is a sync-packet received, then the compare-register in the PIT will be
	 * modified for one tic.
	 * in the IRQ-handler of the PIT, this register has to be restored to the default value.
	 */



// global variables:
	static Pin pinButton = PIN_PUSHBUTTON_1;
	static Pin pinFastSync = PIN_FAST_SYNC;
	static Pin pinSlowSync = PIN_SLOW_SYNC;
	static Pin pinPowerLed1 = PIN_POWER_LED1;
	static Pin pinPowerLed2 = PIN_POWER_LED2;
		SETTINGS* set=NULL;
	static uint8_t powerLedPeriod1 = 0xff;
	static uint8_t powerLedPeriod2 = 0xff;


	static SETTINGS defaultSettingsBB =
	{
		.version = SETTINGS_VERSION, // version number of settings
		.boardType = SET_BT_BoatBox,
		.boardNumber = 0,
		.address_433M = RF_ADDR_INVALID,
		.address_2G4 = 2, // this board is a slave. slave with address 3 must be reconfigured!
		.flashKey = SETTINGS_FLASH_KEY
	};

	static SETTINGS defaultSettingsTS =
	{
		.version = SETTINGS_VERSION, // version number of settings
		.boardType = SET_BT_TriggerStation,
		.boardNumber = 0,
		.address_433M = 2, // this board is a 433MHz-Slave. A slave with address 3 must be reconfigured!
		.address_2G4 = 0, // this board is a 2.4GHz-Master. master with address 1 must be reconfigured!
		.flashKey = SETTINGS_FLASH_KEY
	};

	static SETTINGS defaultSettingsTB =
	{
		.version = SETTINGS_VERSION, // version number of settings
		.boardType = SET_BT_TimeBase,
		.boardNumber = 0,
		.address_433M = 0, // this board is the master
		.address_2G4 = RF_ADDR_INVALID,
		.flashKey = SETTINGS_FLASH_KEY
	};

	static SETTINGS defaultSettingsMB = // for matrix board
	{
		.version = SETTINGS_VERSION, // version number of settings
		.boardType = SET_BT_MatrixBoard,
		.boardNumber = 0,
		.address_433M = 4, // this board is a 433MHz-Slave. A slave with address 5 must be reconfigured!
		.address_2G4 = RF_ADDR_INVALID,
		.flashKey = SETTINGS_FLASH_KEY
	};



// function declarations:
	static void ConfigureSys();
	static void ConfigureTC0();
	static void ConfigureTC1();
	static void ConfigureADC();


/***			ISR handler functions			***/

/*
 * Handler for PIT (periodical interval timer) and Watchdog Timer interrupt.
 * Increments the timestamp counter.
 */
__attribute__ ((section (".ramfunc")))
void ISR_Sys(void)
{
	// Check the PIT status register
	if (PIT_GetStatus() & AT91C_PITC_PITS)
	{
		// Read the PIVR to acknowledge interrupt and get number of ticks
		time_100us += (TIC_INTERVAL_us/100) * (PIT_GetPIVR() >> 20);

		// set compare value back to its original value,
		// but corrected by the software PLL algorithm.
		AT91C_BASE_SYS->PITC_PIMR = PIT_MODE_REG + SoftPLL_step();

		// do not call the RF_Process() directly but in main when this flag is set
		pitFlag = true;

		PULSE_FAST_SYNC;
		if((time_100us & (((uint32_t)1<<10)-1)) == 0)
		{
			PULSE_SLOW_SYNC;
		}else
		{
			// Todo: access 2 registers, to compensate the time consumption
		}

		// NOTE: LED-pulses are fixed to global system time, so we can mask the received pulses
		// to ignore invalid impulses!
		// The global system time has a syncronizity jitter of less than 5us.

		if((powerLedPeriod1!=0xff) && (time_100us%powerLedPeriod1 == 0))
		{
			POWER_LED1_ON;
			AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKEN | AT91C_TC_SWTRG; // enable reset timer and start clock
		}

		if((powerLedPeriod2!=0xff) && (time_100us%powerLedPeriod2 == 0))
		{
			POWER_LED2_ON;
			AT91C_BASE_TC1->TC_CCR = AT91C_TC_CLKEN | AT91C_TC_SWTRG; // enable reset timer and start clock
		}
	}
}

/*
 * Handler for TC0 (Timer/Counter 0).
 * Shuts down the Power-LED1 after some microseconds.
 */
__attribute__ ((section (".ramfunc")))
void ISR_TC0(void)
{
	uint32_t status = AT91C_BASE_TC0->TC_SR;

	if(status & AT91C_TC_CPAS)
	{
		POWER_LED1_OFF;
		//AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKDIS; // disable clock
	}
}

/*
 * Handler for TC1 (Timer/Counter 1).
 * Shuts down the Power-LED2 after some microseconds.
 */
__attribute__ ((section (".ramfunc")))
void ISR_TC1(void)
{
	uint32_t status = AT91C_BASE_TC1->TC_SR;

	if(status & AT91C_TC_CPAS)
	{
		POWER_LED2_OFF;
		//AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKDIS; // disable clock
	}
}

/***				main function			***/
/*
 * Structure of the Software:
 *
 * Timer-Counter 0/1 are used for the Power-LEDs 1/2
 *
 *  Main:
 * 		Configure DBGU and PIT
 * 		Load Config from flash
 * 			(which type of board and board number, addresses)
 * 		switch into separate functions, which do the things necessary for each board:
 *
 * 	Time-Base: (one unit)
 * 		Init TRX-Module (433MHz)
 * 		Send periodically sync packets
 * 		Listen to settime-command from the console
 * 		forward all incoming rf-packets to the DBGU
 *
 * 	Trigger-Station: (2 units)
 * 		process the power LED PWM
 * 		listen to sync packets from the Time-Base
 * 		send sync packets to the Boat-Boxes
 * 		send alive-signal with battery status
 * 		forward RF-packets from the Boat-Boxes to the Time-Base
 *
 * 	Boat-Box: (2 units)
 * 		listen to sync packets from the Trigger-Stations
 * 		listen for modulated light-reception
 * 		send time-stamp packets with Boat-Box-ID
 * 		send alive-signal with battery status
 * 		permanently send RSSI values with Trigger-Station-ID
 *
 * 	Matrix-Board: (2 units)
 * 		listen to sync packets from the Time-Base
 * 		send alive-signal with battery status
 *
 */


int main()
{
	// configure debug unit
	TRACE_CONFIGURE(DBGU_STANDARD, 115200, BOARD_MCK);
	printf("Project: %s\n", OUTPUT);
	printf("    compiled on %s at %s with TRACE_LEVEL = %d\n", __DATE__, __TIME__, TRACE_LEVEL);
	printf("    using time-slots: %dus@433MHz, %dus@2.4GHz\n", TICS_PER_SLOT_433M*100, TICS_PER_SLOT_2G4*100);

	// set up receive DMA for DBGU:
	DBGUPDC_Init();

	TRACE_DEBUG("Configure Watchdog\n");
	AT91C_BASE_WDTC->WDTC_WDMR = // this register can only be written once.
			// therefore disabling the WDT in the board_lowlevel.c has to be removed.
				0x0FF << 0 | // watchdog timer value (=1 second)
				0 << 12 | // interrupt disable (system IRQ)
				1 << 13 | // watchdog sends signal to reset controller on underflow
				0xFFF << 16; // watchdog delta timer value (this feature is not used)


	TRACE_DEBUG("Configure LEDs\n");
	LED_Configure(LED_DS1);
	LED_Configure(LED_DS2);
	LED_Configure(LED_DS3);
	LED_Configure(LED_DS4);
	//LED_Configure(LED_DS5);
	TRACE_DEBUG("Configure Reset Button\n");
	RSTC_SetUserResetEnable(1); // enable reset button
	TRACE_DEBUG("Init PIO Interrupts\n");
	PIO_InitializeInterrupts(0); // pin iqr priority
	TRACE_DEBUG("Configure User Button\n");
	PIO_Configure(&pinButton, 1);
	TRACE_DEBUG("Configure Sync Pins\n");
	PIO_Configure(&pinFastSync, 1);
	PIO_Configure(&pinSlowSync, 1);


	TRACE_DEBUG("Initialize Flash Driver...\n");
	FLASHD_Initialize(BOARD_MCK);
	TRACE_DEBUG("sizeof(SETTINGS)=%d\n", (int) sizeof(SETTINGS));

	TRACE_DEBUG("Configure System-Timer\n");
	ConfigureSys(); // configure pit; is needed by the CC2500_init()


	TRACE_DEBUG("Configure ADC\n");
	ConfigureADC();

	set = myalloc(sizeof(SETTINGS));
	if(SET_loadFromFlash(set, NULL) == NULL) // if settings in flash are invalid
	{// ask user for configuration
		printf("Board is not configured. Please enter type of board:\n");
		printf("(1) Time Base\n");
		printf("(2) Trigger Station\n");
		printf("(3) Boat Box\n");
		printf("(4) Matrix Board\n");

		bool configuredFlag = true;

		uint32_t ledCount=0;
		do
		{
			KICK_DOG;
			while(!DBGUPDC_IsRxReady())
			{
				KICK_DOG;
				if(ledCount==100000)		// LED blinking
				{
					ledCount=0;
					LED_Toggle(LED_DS1);
				}
				ledCount++;
			} // wait for input
			char ch = DBGUPDC_GetChar(NULL);

			configuredFlag = true;
			if	(ch == '1')
			{
				set=&defaultSettingsTB;
				//set->boardType = SET_BT_TimeBase;
			}
			else if(ch == '2')
			{
				set=&defaultSettingsTS;
				//set->boardType = SET_BT_TriggerStation;
			}
			else if(ch == '3')
			{
				set=&defaultSettingsBB;
				//set->boardType = SET_BT_BoatBox;
			}else if(ch == '4')
			{
				set=&defaultSettingsMB;
				//set->boardType = SET_BT_MatrixBoard;
			}else
			{
				configuredFlag = false;
			}
		}while(!configuredFlag);
		set->boardNumber = 0;


		if(set->boardType != SET_BT_TimeBase && set->boardNumber == 0)
		{
			bool configuredFlag = false;
			do
			{
				printf("Enter board number 01 - %d (always type 2 digits):\n", NUM_OF_BOAT_BOXES);

				char ch1, ch2;

				KICK_DOG;

				do
				{
					while(!DBGUPDC_IsRxReady()) // wait for input
					{
						KICK_DOG;
					}
					// read first digit
					ch1=DBGUPDC_GetChar(NULL);
				}
				while(ch1<'0' || ch1>'9');		// remove eventually additional chars e.g. linefeed


				KICK_DOG;
				while(!DBGUPDC_IsRxReady()) // wait for input
				{
					KICK_DOG;
				}

				// read second digit
				ch2=DBGUPDC_GetChar(NULL);

				// convert into decimal number
				if(ch1>='0' && ch1<='9' && ch2>='0' && ch2<='9')
				{
					int num=10*(int)(ch1-'0')+(ch2-'0');

					// check range
					if(num>=1 && num<=NUM_OF_BOAT_BOXES)
					{
						set->boardNumber=num;
						configuredFlag=true;
					}
				}
			}while(!configuredFlag);
		}

		switch(set->boardType)
		{
			case SET_BT_TimeBase:
			{
				set->address_433M = RF_ADDR_MASTER_0; // 0
			}break;
			case SET_BT_TriggerStation:
			{
				set->address_433M = set->boardNumber+1; // 2/3 (slave)
				set->address_2G4 = set->boardNumber-1; // 0/1 (master)
			}break;
			case SET_BT_BoatBox:
			{
				set->address_2G4=set->boardNumber+1; // 2/3/...
			}break;
			case SET_BT_MatrixBoard:
			{
				set->address_433M = set->boardNumber-1+4; // 4/5
			}break;
		}

		SET_writeToFlash(set); // Call this function only after reconfiguration!!
			// otherwise the data in flash can be corrupted on contact-bouncing of the power-plug,
			// when the board looses the power supply, when the flash has been erased already.
	}


	// set Power-LED periods
	if(set->boardNumber == 1 && set->boardType == SET_BT_TriggerStation)
	{
		powerLedPeriod1 = POWER_LED_PERIOD_START/100; // in 100us steps (900us)
		powerLedPeriod2 = 0xff; // no external powerled on start
	}else if(set->boardNumber == 2 && set->boardType == SET_BT_TriggerStation)
	{
		powerLedPeriod1 = POWER_LED_PERIOD_GOAL1/100; // in 100us steps (1000us)
		powerLedPeriod2 = POWER_LED_PERIOD_GOAL2/100; // in 100us steps (1100us)
	}
	else
	{
		// no powerled at all
		powerLedPeriod1 = 0xff;
		powerLedPeriod2 = 0xff;
	}


	switch(set->boardType)
	{
		case SET_BT_TimeBase:
		{
			RunTimeBase(set);
		}break;
		case SET_BT_TriggerStation:
		{
			TRACE_DEBUG("Configure Power LED Pin\n");
			PIO_Configure(&pinPowerLed1, 1);
			PIO_Configure(&pinPowerLed2, 1);
			POWER_LED1_OFF;
			POWER_LED2_OFF;

			TRACE_DEBUG("Configure Timers for Power-LED\n");
			ConfigureTC0();
			ConfigureTC1();
			RunTriggerStation(set);
		}break;
		case SET_BT_BoatBox:
		{
			RunBoatBox(set);
		}break;
		case SET_BT_MatrixBoard:
		{
			RunMatrixBoard(set);
		}break;
	}

	return 0;
}


// configure PIT on runtime to the used sample rate
// do the sampling in the PIT-ISR
static void ConfigureSys()
{
	// Initialize the PIT to the desired frequency
	PIT_DisableIT();
	PIT_Init(TIC_INTERVAL_us, BOARD_MCK / 1000000);

	// Configure interrupt on PIT
	AIC_DisableIT(AT91C_ID_SYS);
	AIC_ConfigureIT(AT91C_ID_SYS, AT91C_AIC_PRIOR_HIGHEST, ISR_Sys);
	AIC_EnableIT(AT91C_ID_SYS);
	PIT_EnableIT();

	// Enable the pit
	PIT_Enable();
	time_100us = 0;
}

// Configure Timer/Counter for generating Pulses for the Power-LED1
static void ConfigureTC0()
{
	PMC_EnablePeripheral(AT91C_ID_TC0);
	// TC Block Mode Register is ok with default settings

	// TC Channel Mode Register: Waveform Mode
	AT91C_BASE_TC0->TC_CMR =
			(0<<0)| // Timer_Clock_0 = MCK/2
			(1<<6)| // stop clock, when reaching Register C compare
			(1<<7)| // Counter clock is disabled when counter reaches RC.
			(1<<15); // Waveform Mode


	// Initialize the TC0 to the desired period
	// MCK = 48MHz, T_LED = 10us, Timer-CLK = MCK/2 = 24MHz --> Compare-Value = 240
	// 100us: RA=2400
	// 20us: RA=480
	// 10us: RA=240
	AT91C_BASE_TC0->TC_RA = 240;
	AT91C_BASE_TC0->TC_RB = 0xff00; // dummy limit
	AT91C_BASE_TC0->TC_RC = 0xfff0; // dummy stop

	// Configure interrupt on PIT
	AIC_DisableIT(AT91C_ID_TC0);
	AIC_ConfigureIT(AT91C_ID_TC0, AT91C_AIC_PRIOR_LOWEST, ISR_TC0);
	AIC_EnableIT(AT91C_ID_TC0);

	AT91C_BASE_TC0->TC_IER = AT91C_TC_CPAS; // enable interrupt on compare-value A
}

// Configure Timer/Counter for generating Pulses for the Power-LED2
static void ConfigureTC1()
{
	PMC_EnablePeripheral(AT91C_ID_TC1);
	// TC Block Mode Register is ok with default settings

	// TC Channel Mode Register: Waveform Mode
	AT91C_BASE_TC1->TC_CMR =
			(0<<0)| // Timer_Clock_0 = MCK/2
			(1<<6)| // stop clock, when reaching Register C compare
			(1<<7)| // Counter clock is disabled when counter reaches RC.
			(1<<15); // Waveform Mode


	// Initialize the TC1 to the desired period
	// MCK = 48MHz, T_LED = 10us, Timer-CLK = MCK/2 = 24MHz --> Compare-Value = 240
	// 100us: RA=2400
	// 20us: RA=480
	AT91C_BASE_TC1->TC_RA = 240;
	AT91C_BASE_TC1->TC_RB = 0xff00; // dummy limit
	AT91C_BASE_TC1->TC_RC = 0xfff0; // dummy stop

	// Configure interrupt on PIT
	AIC_DisableIT(AT91C_ID_TC1);
	AIC_ConfigureIT(AT91C_ID_TC1, AT91C_AIC_PRIOR_LOWEST, ISR_TC1);
	AIC_EnableIT(AT91C_ID_TC1);

	AT91C_BASE_TC1->TC_IER = AT91C_TC_CPAS; // enable interrupt on compare-value A
}


// time_100us is needed!!
static void ConfigureADC()
{
	/*
	 * ADC:
	 * Software Trigger
	 * Zsource = 23,5 kOhm (voltage devider)
	 * no sleep
	 * 10-Bit Mode
	 * ADC-Clock Rate = 1MHz
	 * Start up Time = 20 us
	 * Sample and Hold Time = 2.82 us (calculated for 23.5 kOhm)
	 */
	ADC_Initialize(BOARD_VBAT_BASE, BOARD_VBAT_ID, 0, 0, 0, 1, BOARD_MCK, 1000000, 20, 2820);
	ADC_EnableChannel(BOARD_VBAT_BASE, BOARD_VBAT_CH1);
	ADC_EnableChannel(BOARD_VBAT_BASE, BOARD_VBAT_CH12);
	ADC_EnableChannel(BOARD_VBAT_BASE, BOARD_VBAT_CH123);


	// do initial Conversions, so that ADC_GetConvertedData() delivers valid data
	int n;
	for(n=0; n<5; n++)
	{
		ADC_StartConversion(BOARD_VBAT_BASE);
		TIME_T now = time_100us;
		while(time_100us < now);
	}

	InitVoltageFilters();
}
