/*
 * Karl Zeilhofer
 * 28.9.2010
 *
 * This module implements Streaming functionality for Messages
 * to the DBGU and RS232
 * Until now, only output is supported.
 *
 * adopted on 26.7.2012 for the RFSW Project by Karl Zeilhofer
 *
 */


#ifndef STREAM_H
#define STREAM_H

// devices:
#define RS232 (1)
#define DBGU (2)

#define MSG_BUF_SIZE (100)

typedef struct Message
{
	char data[MSG_BUF_SIZE]; // buffer for deep copies
	uint16_t len;
	enum MsgStatus {MsgSt_Waiting, // is in the message queue, and waiting for transmission
		MsgSt_Transmitting, // the PDC is currently transfering the buffer
		MsgSt_Transmitted, // the buffer is transmitted and, if static buffer was used,
			// the message is waiting to be set to MsgSt_Empty by the caller of enqueueMsgXx()
		MsgSt_Empty
		} state;
	uint8_t isStatic; // flag, if a static buffer was enqueued.
	const void* pBuf; // pointer to the buffer which will be transmitted.
		// either it is Message.data or a given static buffer.
}MESSAGE;

/*
 * use like a printf funktion.
 * device may be RS232 or DBGU or (RS232|DBGU)
 */
void stream(const uint8_t device, const char* formatStr, ...);

void init_streams();
MESSAGE* enqueueMsgRs(const void* buf, uint16_t len, uint8_t staticFlag);
MESSAGE* enqueueMsgDbgu(const void* buf, uint16_t len, uint8_t staticFlag);
uint8_t processMessageQueue();
void printTotalStreamBuffer(const uint8_t device);

#endif // STREAM_H

