/*
 * dbgupdc.h
 *
 *  Created on: 13.07.2012
 *      Author: Karl Zeilhofer
 */

#ifndef DBGUPDC_H_
#define DBGUPDC_H_

#include <stdbool.h>
#include <stdint.h>


void DBGUPDC_Init();
bool DBGUPDC_IsRxReady();
uint8_t DBGUPDC_GetChar(bool* ok);

#endif /* DBGUPDC_H_ */
