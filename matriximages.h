/*
 * matriximages.h
 *
 *  Created on: Aug 14, 2013
 *      Author: Karl Zeilhofer
 */

#ifndef MATRIXIMAGES_H_
#define MATRIXIMAGES_H_
#include "packet.h"

extern uint8_t MatrixImageAkkuLeer[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageKibokoZeitnehmung[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageOff[MATRIX_MODULES][MATRIX_ROWS]; // use this OR-Combined with MatrixImageKibokoZeitnehmung


extern uint8_t MatrixImageMKZ_1a_MKZ[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageMKZ_1b_Mechatronik_KarlZeilhofer[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageMKZ_1c_Steinbach_amZiehberg[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageMKZ_2_Elektronik_Entwicklung[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageMKZ_3_SystemPro__grammierung[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageMKZ_4_Prototypen_Fertigung[MATRIX_MODULES][MATRIX_ROWS];
extern uint8_t MatrixImageMKZ_5_www_zeilhofer_coat[MATRIX_MODULES][MATRIX_ROWS];

#endif /* MATRIXIMAGES_H_ */
