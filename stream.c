/*
 * Karl Zeilhofer
 * 28.9.2010
 *
 * refer to stream.h
 *
 */

#include "defs.h"

#include "stream.h"
#include <stdarg.h>
#include <stdio.h>
#include <utility/trace.h>
#include <string.h>
#include <stdlib.h>


#define DBG_LINE (printf("%s:L%d%c",__FILE__,__LINE__,LF))


#define MSG_QUEUE_SIZE (64)



struct MessageQueue
{
	struct Message messages[MSG_QUEUE_SIZE];
	uint8_t head; // index of msg, which is currently in the pdc
	uint8_t tail; // index of msg, which is the last one added to the queue.
	uint8_t mlTrigger; // if this flag is set, a trigger has to be done in the main loop.
		// otherwise the queue is processed by the RS232-IRQ
};


static struct MessageQueue mqRs;
static struct MessageQueue* pMqRs = &mqRs;
static struct MessageQueue mqDbgu;
static struct MessageQueue* pMqDbgu = &mqDbgu;
static uint8_t initialized=0;


extern AT91PS_DBGU pDbgu;
extern AT91PS_USART pUsartRS232;

AT91PS_USART pUsartDbgu; // using the dbgu only as a usart,
	// it has the same registers as a native usart (all PDC-Registers, Status-Register)
	// refer to the manual for details.


void init_streams()
{
	if(!initialized)
	{
		pMqRs->head=0;
		pMqRs->tail=0;
		pMqDbgu->head=0;
		pMqDbgu->tail=0;

		int n;
		for(n=0; n<MSG_QUEUE_SIZE; n++)
		{
			pMqRs->messages[n].state = MsgSt_Empty;
			pMqDbgu->messages[n].state = MsgSt_Empty;
			pMqRs->messages[n].len = 0;
			pMqDbgu->messages[n].len = 0;
		}

		pUsartDbgu=(AT91PS_USART)pDbgu;
		initialized=1;
	}
}


/*
 * wrapper function for printf()
 * use stream() like a printf(), with the difference to print
 * to the RS232 or DBGU (via PDC)
 */
void stream(const uint8_t device, const char* formatStr, ...)
{
	if(!initialized)
	{
		init_streams();
		TRACE_ERROR("ERROR: stream() was used before initialization!");
	}

	static char buf[101];
	va_list args; // variable to handle "variable length parameter-list"

	va_start(args, formatStr);
	vsnprintf(buf, 100, formatStr, args);
	va_end(args);

	int len = strlen(buf);

	if(device & RS232)
	{
		enqueueMsgRs(buf, len, 0);
	}
	if(device & DBGU)
	{
		enqueueMsgDbgu(buf, len, 0);
	}
}

/*
 * Adds a Message to the message queue for RS232.
 * returns pointer to message on success and NULL on error
 * buf is copied, when staticFlag is false (limited to 100 bytes)
 */
MESSAGE* enqueueMsgRs(const void* buf, uint16_t len, uint8_t staticFlag)
{
	if(!initialized)
	{
		init_streams();
		TRACE_ERROR("ERROR: enqueueMsgRs() was used before initialization!");
	}
	if(!staticFlag && len > MSG_BUF_SIZE)
	{
		TRACE_FATAL("ERROR: enqueueMsgRs(): message-size exceeded limit! - HALT");
	}

	// TODO: Disable IRQs // but this leads to the bug, that the RS232 doesn't receive any more
		// there is a chance, that an IRQ occures, which wants to enqueue a message too.
		// this would mess up the message queue.
	// back up interrupt mask:
	// uint32_t i = *AT91C_AIC_IMR; // interrupt mask register
	//*AT91C_AIC_IDCR = ~(1<<AT91C_ID_SYS); // disable all interrupts except the PIT

	uint8_t tail = pMqRs->tail;
	// if queue is not empty or tail-message is not empty -> increment tail
	if(pMqRs->head != pMqRs->tail || pMqRs->messages[pMqRs->tail].state != MsgSt_Empty)
	{
		tail = (tail+1) % MSG_QUEUE_SIZE;
	}
	// else queue is empty, and tail must not be incremented.
	// (head == tail)

	struct Message* m = &(pMqRs->messages[tail]);
	if(m->state == MsgSt_Empty)
	{
		if(staticFlag) // use static "external" buffer
		{
			m->isStatic = 1;
			m->pBuf = buf;
		}
		else // deep copy the buffer
		{
			m->isStatic = 0;
			memcpy(m->data, buf, len);
			m->pBuf = m->data;
		}
		m->len = len;
		m->state = MsgSt_Waiting;
		pMqRs->tail = tail;
		return m; // return pointer to message structure
	}
	else
	{
		TRACE_ERROR("%s:ERROR: RS232 Message Queue is full; Line = %d",__FILE__,__LINE__);
		return NULL; // error
	}

	// TODO: Reenable Interrupts
	//*AT91C_AIC_IECR = i; // restore interrupt mask
}


/*
 * Adds a Message to the message queue for DBGU.
 * returns 1 on success and 0 on error
 * buf is copied
 */
MESSAGE* enqueueMsgDbgu(const void* buf, uint16_t len, uint8_t staticFlag)
{
	if(!initialized)
	{
		init_streams();
		TRACE_ERROR("ERROR: enqueueMsgDbgu() was used before initialization!");
	}
	if(!staticFlag && len > MSG_BUF_SIZE)
	{
		TRACE_FATAL("ERROR: enqueueMsgDbgu(): message-size exceeded limit! - HALT");
	}

	uint8_t tail = pMqDbgu->tail;
	// if queue is not empty or tail-message is not empty -> increment tail
	if(pMqDbgu->head != pMqDbgu->tail || pMqDbgu->messages[pMqDbgu->tail].state != MsgSt_Empty)
	{
		tail = (tail+1) % MSG_QUEUE_SIZE;
	}

	struct Message* m = &(pMqDbgu->messages[tail]);
	if(m->state == MsgSt_Empty)
	{
		if(staticFlag)
		{
			m->isStatic = 1;
			m->pBuf = buf;
		}
		else
		{
			m->isStatic = 0;
			memcpy(m->data, buf, len);
			m->pBuf = m->data;
		}
		m->len = len;
		m->state = MsgSt_Waiting;
		pMqDbgu->tail = tail;
		return m;
	}
	else
	{
		TRACE_ERROR("%s:ERROR: DBGU Message Queue is full; Line = %d",__FILE__, __LINE__);
		return NULL; // error
	}
}


/*
 * Processes the Queue Lists.
 * If a Message was enqueued, it returns 1
 * else it returns 0.
 */
uint8_t processMessageQueue()
{
	if(!initialized)
	{
		init_streams();
		TRACE_ERROR("ERROR: processMessageQueue() was used before initialization!");
	}

	struct Message* m=NULL;
	uint8_t head;
	struct MessageQueue* mq=NULL;
	AT91PS_USART usart=NULL;

	uint8_t enqueuedSomething=0; // flag

	// RS232:
	if(pUsartRS232->US_CSR & AT91C_US_TXBUFE) // check status
	{
		mq=(struct MessageQueue*)pMqRs; // cast from volatile
		usart=pUsartRS232;

		head=mq->head;
		m = &(mq->messages[head]);
		if(m->state == MsgSt_Transmitting) // if active message was transmitted
			// process next message
		{
			if(m->isStatic)
			{
				m->state = MsgSt_Transmitted;
					// now the state of the message must be set to MsgSt_Empty by the
					// caller of enqueueMsgRs()
			}
			else
			{
				m->state = MsgSt_Empty;
			}
			head = (head+1)%MSG_QUEUE_SIZE;
			mq->head = head;
			m = &(mq->messages[head]);
		}

		if(m->state == MsgSt_Waiting) // if msg is waiting, transmit it
		{
			m->state = MsgSt_Transmitting;
			usart->US_TPR = (uint32_t) m->pBuf;
			usart->US_TCR = m->len;
			usart->US_PTCR = AT91C_PDC_TXTEN;
			enqueuedSomething = 1;
		}
	}

	// DBGU:
	if(pUsartDbgu->US_CSR & AT91C_US_TXBUFE) // if TxBuffer empty
	{
		mq=(struct MessageQueue*)pMqDbgu; // cast from volatile
		usart=pUsartDbgu;

		head=mq->head;
		m = &(mq->messages[head]);
		if(m->state == MsgSt_Transmitting) // if active message was transmitted
			// process next message
		{
			if(m->isStatic)
			{
				m->state = MsgSt_Transmitted;
					// now the state of the message must be set to MsgSt_Empty by the
					// caller of enqueueMsgRs()
			}
			else
			{
				m->state = MsgSt_Empty;
			}
			head = (head+1)%MSG_QUEUE_SIZE;
			mq->head = head;
			m = &(mq->messages[head]);
		}

		if(m->state == MsgSt_Waiting) // if msg is waiting, transmit it
		{
			m->state = MsgSt_Transmitting;
			usart->US_TPR = (uint32_t) m->pBuf;
			usart->US_TCR = m->len;
			usart->US_PTCR = AT91C_PDC_TXTEN;
			enqueuedSomething = 1;
		}
	}
	return enqueuedSomething;
}

/*
 * prints the content of the stream queue, ignoring the state of the messages.
 */
void printTotalStreamBuffer(const uint8_t device)
{

	while(processMessageQueue()); // flush message queue // TODO: here is a bug, regarding
		// waiting for empty transmit buffer. if buffer is not transmitte, no message is enqueued,
		// and therefore processMessageQueue() returns zero, but the queue isn't empty.

	int n;
	int i;
	uint8_t msg[MSG_BUF_SIZE+1];
	uint8_t out[MSG_BUF_SIZE+20+1];

	AT91PS_USART pUsart;
	struct MessageQueue* pMq;

	if(device & RS232)
	{
		pMq = (struct MessageQueue*) pMqRs;
		pUsart =  pUsartRS232;
		n = (pMq->tail+1)%MSG_QUEUE_SIZE;
		i = -(MSG_QUEUE_SIZE-1);

		while(!(pUsart->US_CSR & AT91C_US_TXBUFE)); // wait for finished buffer
		do
		{
			if(!(pMq->messages[n].isStatic)) // skip static messages
				// because of the possible too long message of static buffers
			{
				memcpy(msg, pMq->messages[n].pBuf, pMq->messages[n].len);
				msg[pMq->messages[n].len]=0;
				snprintf((char*)out, MSG_BUF_SIZE+20, "QUOTE[%3d]%s",i,msg);
				pUsart->US_TPR = (unsigned int)out;
				pUsart->US_TCR = strlen((char*)out);
				pUsart->US_PTCR = AT91C_PDC_TXTEN;
			}
			n++;
			i++;
			if(n>=MSG_QUEUE_SIZE)
			{
				n=0;
			}
			while(!(pUsart->US_CSR & AT91C_US_TXBUFE)); // wait for finished buffer
		}while(n!=(pMq->tail+1)%MSG_QUEUE_SIZE);
	}

	if(device & DBGU)
	{
		pMq = (struct MessageQueue*) pMqDbgu;
		pUsart =  pUsartDbgu;
		n = (pMq->tail+1)%MSG_QUEUE_SIZE;
		i = -(MSG_QUEUE_SIZE-1);

		while(!(pUsart->US_CSR & AT91C_US_TXBUFE)); // wait for finished buffer
		do
		{
			if(!(pMq->messages[n].isStatic)) // skip static messages
				// because of the possible too long message of static buffers
			{
				memcpy(msg, pMq->messages[n].pBuf, pMq->messages[n].len);
				msg[pMq->messages[n].len]=0;
				snprintf((char*)out, MSG_BUF_SIZE+20, "QUOTE[%3d]%s", i,msg);
				pUsart->US_TPR = (unsigned int)out;
				pUsart->US_TCR = strlen((char*)out);
				pUsart->US_PTCR = AT91C_PDC_TXTEN;
			}
			n++;
			i++;
			if(n>=MSG_QUEUE_SIZE)
			{
				n=0;
			}
			while(!(pUsart->US_CSR & AT91C_US_TXBUFE)); // wait for finished buffer
		}while(n!=(pMq->tail+1)%MSG_QUEUE_SIZE);
	}
}

