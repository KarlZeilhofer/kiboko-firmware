/*
 * settings.c
 *
 *  Created on: 06.07.2012
 *      Author: Karl Zeilhofer
 */

// TODO: make this modular usable

#include "settings.h"

/*
 * loads settings into set
 * if no valid settings are available from flash,
 * defaultSettings in parameter are used.
 */
SETTINGS* SET_loadFromFlash(SETTINGS* set, const SETTINGS* defaultSettings)
{
	SETTINGS* flashSet;
	flashSet = (SETTINGS*) SETTINGS_FLASH_ADDR; // flashSet set to the structure in the flash memory

	if(set == NULL)
	{
		TRACE_ERROR("set-pointer in %s() is NULL\n", __FUNCTION__)
	}

	// if valid flash data:
	if (flashSet->flashKey == SETTINGS_FLASH_KEY && flashSet->version == SETTINGS_VERSION)
	{
		// then the settings in flash are valid.

		// copy structure from flash to ram to make it writable:
		*set = *flashSet;
		TRACE_INFO("Loaded Valid Settings from Flash!\n");
		return set;
	}
	else if(defaultSettings)// settings in flash are invalid, load default settings:
	{
		TRACE_INFO("Invalid Settings in Flash\n");

		*set = *defaultSettings;
		printf("Using Default Settings!\n");
		SET_writeToFlash(set);
		return set;
	}else
	{
		TRACE_INFO("Invalid Settings in Flash, no default Settings given\n");
		return NULL;
	}
	return NULL;
}

void SET_writeToFlash(SETTINGS* set)
{
	TRACE_DEBUG("Write Settings from address 0x%08x to Flash...", (unsigned int) set);

	// back up interrupt mask:
	uint32_t i = AT91C_BASE_AIC->AIC_IMR; // interrupt mask register
	AT91C_BASE_AIC->AIC_IDCR = 0xffffffff; // disable all interrupts

	if(!FLASHD_Write(SETTINGS_FLASH_ADDR, set, sizeof(SETTINGS)))
	{
		TRACE_INFO_WP("    [OK]\n");
		TRACE_INFO_WP("    Check Settings in RAM against FLASH...");
		if(SET_compare(set, (SETTINGS*)SETTINGS_FLASH_ADDR))
		{
			TRACE_INFO_WP("[OK]");
		}else
		{
			TRACE_ERROR("\n Error on comparing Settings in RAM to FLASH (in %s, Line %d)", __FILE__, __LINE__);
		}
	}
	else
	{
		TRACE_ERROR("\nError on writing set to flash.\n");
	}
	AT91C_BASE_AIC->AIC_IECR = i; // restore interrupt mask
}

/*
 * typedef struct structSettings
{
	int32_t version; // version number of settings
	SET_BOARD_TYPE boardType;
	uint8_t boardNumber; // 1 or 2
	uint8_t address_433M;
	uint8_t address_2G4;

	uint8_t ALIGN_TO_FOUR; // dummy entry, to align to four bytes.

	uint32_t flashKey; // check bytes for successful written bytes to flash
} SETTINGS;
 */

void SET_print(SETTINGS* set)
{
	printf("SETTINGS:\n"
			"Version = %d\n"
			"boardType = %d\n"
			"boardNumber = %d\n"
			"address_433M = %d\n"
			"address_2G4 = %d\n\n",
			(int)(set->version), (int)(set->boardType), (int)(set->boardNumber),
			(int)(set->address_433M), (int)(set->address_2G4));
}


// returns 1 on success, otherwise 0
uint8_t SET_compare(SETTINGS* set1, SETTINGS* set2)
{
	if(set1->version != set2->version) return 0;
	if(set1->boardType != set2->boardType) return 0;
	if(set1->boardNumber != set2->boardNumber) return 0;
	if(set1->address_433M != set2->address_433M) return 0;
	if(set1->address_2G4 != set2->address_2G4) return 0;
	if(set1->flashKey != set2->flashKey) return 0;

	return 1;
}
