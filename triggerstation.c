/*
 * triggerstation.c
 *
 *  Created on: 11.07.2012
 *      Author: Karl Zeilhofer
 */

/*
 * LED: is ON, when synchronized with the Time-Base, else OFF
 */

#include "defs.h"
#include "rfswshared.h"

#include "triggerstation.h"
#include "myalloc.h"
#include "settings.h"
#include "photodetect.h"
#include "dbgupdc.h"
#include "boatboxbuffer.h"
//#include "stream.h"
#include "packet_functions.h"

#include <rf/rf.h>

/*
 * TRX-Module A: 2.4GHz (Trigger-Station and Boat-Box)
 * TRX-Module B: 433MHz (Time-Base and Trigger-Station)
 */

// Definitions:
	#define AUTO_ALIVE_DELAY 50000 // when TS gets no packet from a BB for more than 5s,
		// it sends autonomously alive-packets
	#define AUTO_ALIVE_PERIOD 5000 //when no BB is heared by the TS, send alive packets every 0.5s

// global volatile variables:
	static volatile bool receiveFlagA = false; // this flag is set by the Pin-changing interrupt, it is cleared in main
//	static volatile TIME_T receiveTimeA_100us;
//	static volatile uint32_t receiveTimeA_subMicro; // in 1/3rd micro second steps
	static volatile bool receiveFlagB = false; // this flag is set by the Pin-changing interrupt, it is cleared in main
	static volatile TIME_T receiveTimeB_100us;
	static volatile uint32_t receiveTimeB_subMicro; // in 1/3rd micro second steps
	static volatile TIME_T tOfLastTriggeredL = TIME_INVALID; // atomic access (32 bit)
	static volatile TIME_T tOfLastTriggeredR = TIME_INVALID; // atomic access (32 bit)

	// Command Bytes (Commands can be sent from the Kiboko-Manager to the RF-Devices)
	// TODO 3: ensure packet data integrety!!!, values are changed in an ISR!
	static volatile uint16_t cmdReceiver=COMMAND_INVALID_RECEIVER; // MSB=0...433MHz, MSB=1...2.4GHz devices
	static volatile uint8_t command=RCmd_INVALID; // Command Byte (Refer to enum Radio_Command)
	static volatile uint16_t commandData=0; // Command Byte (Refer to enum Radio_Command)
	static volatile bool aCmdHasToBeSent = false;


// global variables:
	static Pin pinButton = PIN_PUSHBUTTON_1;
	static Pin pinsLightSensors[] ={PINS_LIGHT_SENSORS};




// global objects:
	static TRX_Object* trxA=NULL;
	static TRX_Object* trxB=NULL;
	static RF_Object* rfA=NULL;
	static RF_Object* rfB=NULL;

// function declarations:
	static void RfA_ReceiveHandler(RF_Object* rfObj);
	static void RfB_ReceiveHandler(RF_Object* rfObj);





// Any Packet was received, forward IRQ to software RF-Module
static void ISR_TrxA_GD0_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
	// DCL_Gd0ChangedHandler(trxA);

	TRACE_DEBUG("GDO0 changed\n");
	if (PIO_Get(&trxA->settings.pins.gd0)) // rising edge
	{
		// do not call the receive handler directly but in main when this flag is set.
		receiveFlagA = true;
	}
	else // falling edge
	{

	}
}

// Any Packet was received, forward IRQ to software RF-Module
static void ISR_TrxB_GD0_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
	// DCL_Gd0ChangedHandler(trxB);

	TRACE_DEBUG("GDO0 changed\n");
	if (PIO_Get(&trxB->settings.pins.gd0)) // rising edge
	{
		// do not call the receive handler directly but in main when this flag is set.
		receiveFlagB = true;
		receiveTimeB_100us = time_100us;
		receiveTimeB_subMicro = AT91C_BASE_SYS->PITC_PIIR & 0xfffff;
		if(receiveTimeB_100us != time_100us) // if time_100us overflow during this, then do it again
		{
			receiveTimeB_subMicro = AT91C_BASE_SYS->PITC_PIIR & 0xfffff;
			receiveTimeB_100us = time_100us;
		}
	}
	else // falling edge
	{

	}
}

static void ISR_Button_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
}

static void ISR_LightSensorL_changed(const Pin* pPin)
{
	if(PIO_Get(pPin) == 0) // falling edge
	{
		//stream(DBGU, "light sensor left triggerd\n");
		if(time_100us > tOfLastTriggeredL + IGNORE_TRIGGER_DURATION_TS_100us)
		{
			tOfLastTriggeredL = time_100us;
		}
	}
}
static void ISR_LightSensorR_changed(const Pin* pPin)
{
	if(PIO_Get(pPin) == 0) // falling edge
	{
		//stream(DBGU, "light sensor right triggerd\n");
		if(time_100us > tOfLastTriggeredR + IGNORE_TRIGGER_DURATION_TS_100us)
		{
			tOfLastTriggeredR = time_100us;
		}
	}
}

void RunTriggerStation(SETTINGS* set)
{

	DCL_Command cmd;
	printf("Running as Trigger-Station (%d)\n\n", set->boardNumber);
	SET_print(set);

	trxA = myalloc(sizeof(TRX_Object));
	trxB = myalloc(sizeof(TRX_Object));
	rfA = myalloc(sizeof(RF_Object));
	rfB = myalloc(sizeof(RF_Object));

	TRACE_DEBUG("Configure User-Button...")
	PIO_Configure(&pinButton, 1);
	PIO_ConfigureIt(&pinButton, ISR_Button_changed);
	PIO_EnableIt(&pinButton);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure TRX Module A (2.4GHz)...");
	TRX_constructor(trxA, TRX_PINSET_A, TRX_CONFIG_CC2500);
	TRX_init(trxA);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure TRX Module B (433MHz)...");
	TRX_constructor(trxB, TRX_PINSET_B, TRX_CONFIG_CC1101);
	TRX_init(trxB);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Init Console...");
	DCL_Init(trxA, trxB);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure GDO0-Interrupt A...");
	PIO_ConfigureIt(&(trxA->settings.pins.gd0), ISR_TrxA_GD0_changed);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure GDO0-Interrupt B...");
	PIO_ConfigureIt(&(trxB->settings.pins.gd0), ISR_TrxB_GD0_changed);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Setup RF-Communication A...");
	RF_Setup(rfA, NUM_OF_SLOTS_2G4, TICS_PER_SLOT_2G4, trxA, set->address_2G4,
			RfA_ReceiveHandler, TRANSMIT_DELAY_100us_2G4, TRANSMIT_DELAY_thirdMicroSec_2G4);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Setup RF-Communication B...");
	RF_Setup(rfB, NUM_OF_SLOTS_433M, TICS_PER_SLOT_433M, trxB, set->address_433M,
			RfB_ReceiveHandler, TRANSMIT_DELAY_100us_433M, TRANSMIT_DELAY_thirdMicroSec_433M);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Enable Interrupts for GDO0A and GDO0B...");
	PIO_EnableIt(&(trxA->settings.pins.gd0));
	PIO_EnableIt(&(trxB->settings.pins.gd0));
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure Light Sensor Pins...");
	PIO_Configure(pinsLightSensors, 2);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure Light Sensor Interrupts...");
	PIO_ConfigureIt(&(pinsLightSensors[0]), ISR_LightSensorL_changed);
	PIO_ConfigureIt(&(pinsLightSensors[1]), ISR_LightSensorR_changed);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Initialize Boat-Box-Buffer...");
	BBB_init();
	TRACE_DEBUG_WP("[OK]\n");


	// if it is the trigger-station 1 (start), set light-sensor trigger-time to invalid!
	if(set->boardNumber == 1){
		tOfLastTriggeredR = TIME_INVALID;
	}

	TRACE_DEBUG("Enable Interrupts for Light Sensors...");
	PIO_EnableIt(&(pinsLightSensors[0]));
	PIO_EnableIt(&(pinsLightSensors[1]));
	TRACE_DEBUG_WP("[OK]\n");



	/*
	 * The rf-object, which has to send a packet next, has to be
	 * processed as soon as possible, after the pitFlag was set.
	 */
	RF_Object* rfFirst = rfA;
	RF_Object* rfSecond = rfB;
	bool isRunning = true;

	while(1)
	{
		KICK_DOG;

		static bool validPacket = false;
		static int packetPriority;
		static ALIVE_PACKET packet;
		static TIME_T recentTransmit=0;


		if(isRunning)
		{
			if(rfA->nextTxTime <= rfB->nextTxTime)
			{
				rfFirst = rfA;
				rfSecond = rfB;
			}else
			{
				rfFirst = rfB;
				rfSecond = rfA;
			}
			while(pitFlag == false); // wait for changed time_100us
			pitFlag = false;
			if(RF_Process(rfFirst)) // if packet was sent
			{
				if(rfFirst == rfA)
				{
					LED_Toggle(LED_DS_TxA);
					// Update battery voltages:
					UpdateVoltages();
				}else
				{
					//LED_Toggle(LED_DS_TxB);
				}
			}
			if(RF_Process(rfSecond)) // if packet was sent
			{
				if(rfSecond == rfA)
				{
					LED_Toggle(LED_DS_TxA);
					// Update battery voltages:
					UpdateVoltages();
				}else
				{
					//LED_Toggle(LED_DS_TxB);
				}
			}

			if(receiveFlagA)
			{
				TRACE_DEBUG("Got receiveFlagA\n");
				receiveFlagA = false;
				if(RF_Receive(rfA))
				{
					LED_Toggle(LED_DS_RxA);
				}
			}
			if(receiveFlagB)
			{
				TRACE_DEBUG("Got receiveFlagB\n");
				receiveFlagB = false;
				if(RF_ReceiveLater(rfB, receiveTimeB_100us, receiveTimeB_subMicro)) // sync the time_100us
				{
					LED_Toggle(LED_DS_RxB);
				}
			}

			if(rfB->synchronized && rfA->fifoFilled == false)
			{
				if(aCmdHasToBeSent) // when we got a command for the RF-Devices from the Kiboko-Manager
				{
					printf("a command has to be sent\n");
					COMMAND_PACKET packet;
					packet.packetType = PACKET_TYPE_COMMAND;
					packet.cmdReceiver = cmdReceiver;
					packet.command = command;
					packet.commandData = commandData;

					uint8_t destAddr=254; // rf address for the 2.4GHz Network, by default invalid
					if((cmdReceiver & 0xFF00) == 0) // if for the 433MHz-Devices
					{
						// should never happen here
						printf("destAddress is in 433MHz Network\n");
					}else // it is for the 2.4GHz devices
					{
						destAddr = cmdReceiver & 0x00FF; // to the destination node in the 2.4GHz Network
						printf("destAddress is in 2.4GHz Network\n");
					}

					if(destAddr!=254)
					{
						if(RF_LoadFifo(rfA, destAddr, (uint8_t*)&packet, sizeof(packet)) != 0) // if no error
						{
							TRACE_WARNING("Error on loading Command-Packet into the FIFO for transmit\n");
						}else
						{
							printf("Sent a radio command via RF\n");
						}
					}

					aCmdHasToBeSent = false; // do not retry a RF_LoadFifo() on an error.
				}else{ // if there is no command to be forwarded...
					// send sync packet
					RF_LoadSync(rfA);
				}
			}

			if(rfB->synchronized && rfB->fifoFilled == false)
			{
				static const TIME_T TransmitDelay = 10000/8; // max. 8 packets per second for "economy" packets

				if(!validPacket)
				{
					packet = BBB_getPacket(&packetPriority);
					validPacket = true;
				}

				if(packetPriority > BBB_NEW_PACKET_PRIORITY || // if high-priority packet or
						time_100us > recentTransmit + TransmitDelay) // enough time elapsed for economy-packet
				{
					recentTransmit = time_100us;

					// set current data from the trigger station:
					packet.batteryVoltageTS = vMin*1000; // decimal fixed point: 1234 = 1.234V
					packet.triggerTimeTS_L = tOfLastTriggeredL;
					packet.triggerTimeTS_R = tOfLastTriggeredR;

					RF_LoadFifo(rfB, RF_ADDR_MASTER_0, (uint8_t*)(&packet), sizeof(ALIVE_PACKET));
					validPacket = false; // fetch a new packet from the boat-box-buffer next time.
				}
			}

			if(rfB->synchronized)
			{
				LED_Set(LED_DS1);
			}else
			{
				LED_Clear(LED_DS1);
			}

			// rfB receives the sync-packets from the timebase, so rfA has to have the same value for it's sync-flag.
			rfA->synchronized=rfB->synchronized;
			rfA->lastSyncTime=rfB->lastSyncTime;
		} // end if(isRunning)


		if(DBGUPDC_IsRxReady()) // if Rx has bytes to be read
		{
			cmd = DCL_FeedWithChar(DBGUPDC_GetChar(NULL));
			if(cmd == DCL_cStop)
			{
				isRunning = false;
			}else if( cmd == DCL_cStart)
			{
				isRunning = true;
			}else if(cmd == DCL_cPrintrf)
			{
				printf("\n\nrfA:\n");
				RF_Print(rfA);
				printf("\n\nrfB:\n");
				RF_Print(rfB);
			}else if(cmd == DCL_cVolt)
			{
				UpdateVoltages();
				PrintVoltages();
			}else if(cmd == DCL_cDebug)
			{
				// some debug outputs regarding the spontaneous hang up of transmitting packets
				printf("rfB synchronized = %d\n", rfB->synchronized);
				printf("rfB fifoFilled = %d\n", rfB->fifoFilled);
				printf("validPacket = %d\n", validPacket);
				printf("packetPriority = %d\n", packetPriority);
				PACKET_printAlivePacket(&packet);
				printf("recentTransmit = "); PrintTime(recentTransmit); printf(" (%u)\n", (unsigned int)recentTransmit);
				printf("current time: "); PrintTime(time_100us); printf(" (%u)\n", (unsigned int)time_100us);
				printf("\n");
				BBB_printBuffer();
			}
		}
	}// end of endless loop
}

// Data Packet was Received for this node
// this handler will be called by the software RF-Module
// do something with the data
static void RfA_ReceiveHandler(RF_Object* rfObj)
{
	TRACE_DEBUG("Data Packet Received, len = %d\n", rfObj->rxLength);
	// forward packets to the Time-Base

	ALIVE_PACKET* packetInBuffer;
	ALIVE_PACKET p;

	packetInBuffer = (ALIVE_PACKET*)(rfObj->rxBuffer+3); // skip LEN + SOURCE + DESTINATION
	p = *packetInBuffer; // copy packet to local memory, so the rxBuffer can be used again

	int rssiBin = *(rfObj->rxBuffer+3+sizeof(ALIVE_PACKET));

	p.binRssiBB = rssiBin; // store the rssi value
	BBB_insertPacket(p); // store the received packet here in the buffer
		// transmit it in the main-loop
		// TODO 3: ensure packet data integrety!!!, values are changed in an ISR!
		// perhaps this is the source of spurious time stamps!
}

// Data Packet was Received for this node
// this handler will be called by the software RF-Module
// do something with the data
static void RfB_ReceiveHandler(RF_Object* rfObj)
{
	TRACE_DEBUG("Data Packet Received, len = %d\n", rfObj->rxLength);
	// here is space for handling commands in the Trigger-Station
	// or forward commands to the Boat-Box

	COMMAND_PACKET* packetInBuffer;
	COMMAND_PACKET p;

	packetInBuffer = (COMMAND_PACKET*)(rfObj->rxBuffer+3); // skip LEN + SOURCE + DESTINATION
	p = *packetInBuffer; // copy packet to local memory, so the rxBuffer can be used again

	printf("got a packet\n");

	if(p.packetType == PACKET_TYPE_COMMAND)
	{
		printf("got a command packet\n");
		printf("  %d,%d,%d\n",cmdReceiver, command, commandData);

		if(((p.cmdReceiver&0xFF00) == 0) && // if it is for the 433MHz devices AND
			(p.cmdReceiver&0x00FF) == rfObj->address) // it is for me
		{
			// command is for this device!
			switch(p.command)
			{
				case RCmd_RESET:
					printf("RESET by command\n");
					while(1); // wait for death (Watchdog Timeout)
					break;
				case RCmd_INVALID:
					// nothing to do here!
					break;
			}
		}
		else
		{
			printf("got a command for forwarding\n");

			// store data to be forwarded.
			cmdReceiver = p.cmdReceiver;
			command = p.command;
			commandData = p.commandData;

			aCmdHasToBeSent = true;
		}
	}
}



