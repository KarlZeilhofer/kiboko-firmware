/*
 * boatboxbuffer.h
 *
 *  Created on: Aug 13, 2013
 *      Author: Karl Zeilhofer
 *
 *
 *  This module implements a buffer, which holds all the most recent data from each
 *  boat-box in the rf-network.
 *  With this, the transmition delay can be shortened, because the time-slot of the
 *  Boat-Boxes is independent from the time-slot of the time-base.
 *
 */

#ifndef BOATBOXBUFFER_H_
#define BOATBOXBUFFER_H_

#include "packet.h"

#define BBB_NEW_TIMESTAMP_PRIORITY 20 // priority value for packets with new time stamp
	// it represents the immediate repetition of transmission.
#define BBB_NEW_PACKET_PRIORITY 5


void BBB_init();
void BBB_insertPacket(ALIVE_PACKET p);
ALIVE_PACKET BBB_getPacket(int* priority);
void BBB_printBuffer();



#endif /* BOATBOXBUFFER_H_ */
