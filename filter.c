/*
 * Karl Zeilhofer
 * 23.8.2010
 *
 *
 * Version History:
 * 	23.8.2010:	V0.1
 * 				First implementations
 * 4.11.2010: added '__attribute__ ((section (".ramfunc")))'
 *
 */

#include "filter.h"
#include "myalloc.h"




// create new filter-object
// uses myalloc()
// initializes the coefficiens with an moving average filter
FILT_D* FILT_New(int orderNum, int orderDenom)
{
	FILT_D* f = myalloc(sizeof(FILT_D));

	f->orderNum = orderNum;
	f->orderDen = orderDenom;

	// filter coefficients:
	f->b = myalloc(sizeof(FILT_T)*(orderNum+1));
	f->a = myalloc(sizeof(FILT_T)*(orderDenom+1));

	// samples buffer:
	f->x = myalloc(sizeof(FILT_D)*(orderNum+1));
	f->y = myalloc(sizeof(FILT_D)*(orderDenom+1));

	// clear buffers
	for(int i=0; i<(orderNum+1); i++)
	{
		f->b[i]=1;
		f->x[i]=0;
	}
	for(int i=0; i<(orderDenom+1); i++)
	{
		f->a[i]=0;
		f->y[i]=0;
	}

	f->a[0]=1.0f; // must be one!

	return f;
}

void FILT_SetCoeffs(FILT_D* f, FILT_T* b, FILT_T* a)
{
	for(int i=0; i<(f->orderNum+1); i++)
	{
		f->b[i] = b[i];
	}
	for(int i=0; i<(f->orderDen+1); i++)
	{
		f->a[i] = a[i];
	}
}

__attribute__ ((section (".ramfunc")))
FILT_T FILT_Filter(FILT_D* f, FILT_T x0)
{
	int n;
	FILT_T fir, iir; // fir- and iir-part of the calculation

	f->x[0]=x0;


	fir=0;
	for(n=0; n<f->orderNum+1; n++)
	{
		fir += f->x[n]*f->b[n];
	}
	iir=0;
	for(n=1; n<f->orderDen+1; n++)
	{
		iir += f->y[n]*f->a[n];
	}

	f->y[0]=fir-iir;

	// shift the input sample buffers
	for(n=f->orderNum; n>=0; n--)
	{
		f->x[n] = f->x[n-1];
	}

	// shift the output sample buffer
	for(n=f->orderDen; n>=0; n--)
	{
		f->y[n] = f->y[n-1];
	}

	return f->y[1];
}

