// Karl Zeilhofer
// 18.1.2014


#include "packet_functions.h"
#include "rfswshared.h"

/*
    TIME_T triggerTimeBB;	// send the last trigger time
	TIME_T triggerTimeTS_L; // recent trigger time of left light sensor (or at TS1)
	TIME_T triggerTimeTS_R; // recent trigger time of right light sensor (just in the case of TS2)

	uint16_t batteryVoltageTS;	// battery voltage of Trigger-Station in mV
	uint16_t batteryVoltageBB;	// battery voltage of Boat-Box in mV

	uint8_t boatBoxID; // 1...20
	uint8_t binRssiBB; // binary rssi-value of a packet coming from a Boat-Box
		// is set at the Trigger-Station

	uint8_t stationTriggeredAt; // where the power-led has been detected (Start=1/Goal_left=2/Goal_right=3)


	uint8_t dummy1;		// for alignment. this field is never set and has undefined value

	uint16_t packetType; // refer to enum Packet_Type
		// not used until now (23.8.2013)
	uint16_t dummy2;
 */
void PACKET_printAlivePacket(ALIVE_PACKET* p)
{
	printf("Alive-Packet:\n");
	printf("    triggerTimeBB = "); PrintTime(p->triggerTimeBB); printf(" (%u)\n", (unsigned int)p->triggerTimeBB);
	printf("    triggerTimeTS_L = "); PrintTime(p->triggerTimeTS_L); printf(" (%u)\n", (unsigned int)p->triggerTimeTS_L);
	printf("    triggerTimeTS_R = "); PrintTime(p->triggerTimeTS_R); printf(" (%u)\n", (unsigned int)p->triggerTimeTS_R);
	printf("    batteryVoltageTS = %umV\n", p->batteryVoltageTS);
	printf("    batteryVoltageBB = %umV\n", p->batteryVoltageBB);
	printf("    boatBoxID = %u\n", p->boatBoxID);
	printf("    binRssiBB = %u\n", p->binRssiBB);
	printf("    stationTriggeredAt = ");
	switch(p->stationTriggeredAt)
	{
	case 1: printf("Start\n");break;
	case 2: printf("Gloal Left\n");break;
	case 3: printf("Goal Right\n");break;
	default: printf("None\n");
	}
}
