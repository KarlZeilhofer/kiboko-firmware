/*
 * rfswshared.c
 *
 *  Created on: 25.07.2012
 *      Author: karl
 */

#include "rfswshared.h"
#include "filter.h"


// shared global variables:
	volatile TIME_T time_100us = 0; // global system time in 100-micro seconds
	volatile bool pitFlag = false; // this flag is set by the PIT-interrupt, it is cleared in main


	// voltages of each cell:
	float v1 = 0;
	float v2 = 0;
	float v3 = 0;
	float vMin = 0;
	static uint16_t adc1;
	static uint16_t adc12;
	static uint16_t adc123;

	AT91S_DBGU* pDbgu = AT91C_BASE_DBGU;
	AT91S_USART* pUsartRS232 = BOARD_RS232_USART_BASE;
	AT91S_USART* pUsartRS232_ALGE = BOARD_RS232_ALGE_USART_BASE;



#define ORDER_NUM 5
#define ORDER_DENOM 5

FILT_D* filter1 = NULL;
FILT_D* filter12 = NULL;
FILT_D* filter123 = NULL;

void InitVoltageFilters()
{
	FILT_T b[]=B_BW5_2K_50;
	FILT_T a[]=A_BW5_2K_50;

	filter1 = FILT_New(ORDER_NUM,ORDER_DENOM);
	FILT_SetCoeffs(filter1, b, a);
	filter12 = FILT_New(ORDER_NUM,ORDER_DENOM);
	FILT_SetCoeffs(filter12, b, a);
	filter123 = FILT_New(ORDER_NUM,ORDER_DENOM);
	FILT_SetCoeffs(filter123, b, a);
}


// this functions samples the battery-voltages and filters it with the
// butterworth filter.
// this needs about 210us!!! (Speed-optimization, 48MHz)
// call this function, just after a packet has been sent.
void UpdateVoltages()
{
	static const float Vref = 3.3; // Volts
	float R1;
	float R2;

	adc1 = ADC_GetConvertedData(BOARD_VBAT_BASE, BOARD_VBAT_CH1) & 0x3FF;
	adc12 = ADC_GetConvertedData(BOARD_VBAT_BASE, BOARD_VBAT_CH12) & 0x3FF;
	adc123 = ADC_GetConvertedData(BOARD_VBAT_BASE, BOARD_VBAT_CH123) & 0x3FF;
	ADC_StartConversion(BOARD_VBAT_BASE); // start sampling for next reading

	// cell 1
	R1 = 10e3;
	R2 = 33e3;
	v1 = (float)adc1*Vref/1024.0f * (R1+R2)/R2;
	//v1 = FILT_Filter(filter1, v1);

	// cell 1+2
	R1 = 68e3;
	R2 = 33e3;
	float v12 = (float)adc12*Vref/1024.0f * (R1+R2)/R2;
	//v12 = FILT_Filter(filter12, v12);

	// cell 1+2+3
	R1 = 68e3;
	R2 = 22e3;
	float v123 = (float)adc123*Vref/1024.0f * (R1+R2)/R2;
	//v123 = FILT_Filter(filter123, v123);

	v2 = v12-v1;
	v3 = v123-v12;

	// find minimum of v1,v2,v3
	float min12;
	if(v1 < v2) min12 = v1;
	else min12 = v2;
	if(min12 < v3) vMin = min12;
	else vMin = v3;
}


// this function returns a pointer to a static memory.
// use it max. 3 times at once in a printf.
char* sprintHMSX(TIME_T t)
{
	/* convert a 100us based time into the format hh:mm:ss:xxxx
	 * h ... hours
	 * m ... minutes
	 * s ... seconds
	 * x ... 100u-seconds rest
	 */

	uint8_t h,m,s;
	static char str[3][25]; // tree blocks with length of 25
	static uint8_t curBuf = 0;

	str[curBuf][0]=0; // terminate string

	h =  t/10000/3600;
	t = t-h*10000*3600;
	m = t/10000/60;
	t = t-m*10000*60;
	s = t/10000;
	t = t-s*10000;

	sprintf(str[curBuf], "%02d:%02d:%02d.%04d", h, m, s, (int)t);

	char* ret = str[curBuf];
	curBuf = (curBuf+1)%3;
	return ret;
}

void PrintVoltages()
{
	float v;

	printf("adc1: %d, adc12: %d, adc123: %d\n", adc1, adc12, adc123);

	v = v1;
	printf("Battery 1: %dmV\n", (int)(v*1000));
	v = v2;
	printf("Battery 2: %dmV\n", (int)(v*1000));
	v = v3;
	printf("Battery 3: %dmV\n", (int)(v*1000));
}


// 0...100% = 3V...4.2V
int VoltageToPercent(float v)
{
	int p = (v-3.0)/1.2f*100 + 0.5f; // do rounding

	// avoid negative percentages.
	if(p<0)
		return 0;
	else
		return p;
}

void PrintTime(TIME_T t) // print a human readable time, without a new-line
{
	uint8_t h,m,s;

	h =  t/10000/3600;
	t = t-h*10000*3600;
	m = t/10000/60;
	t = t-m*10000*60;
	s = t/10000;
	t = t-s*10000; // t holds sub-seconds in 100us-units
	printf("%02d:%02d:%02d.%04d", (int)h, (int)m, (int)s, (int)t);
}

