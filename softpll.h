

#ifndef SOFTPLL_H
#define SOFTPLL_H



#include <stdint.h>

int32_t SoftPLL_step();
void SoftPLL_updateFrequencyOffset(int32_t delta_thirdMicros);

#endif
