/*
 * boatbox.c
 *
 *  Created on: 10.07.2012
 *      Author: Karl Zeilhofer
 */

/*
 * LED: flashes for 0.2s, when a trigger event occurred.
 */


#include "defs.h"
#include "rfswshared.h"

#include "boatbox.h"
#include "myalloc.h"
#include "settings.h"
#include "photodetect.h"
#include "dbgupdc.h"

#include <rf/rf.h>


// global volatile variables:
	static volatile bool receiveFlag = false; // this flag is set by the Pin-changing interrupt, it is cleared in main
	static volatile TIME_T receiveTime_100us;
	static volatile uint32_t receiveTime_subMicro; // in 1/3rd micro second steps
	static volatile bool saveSettings = false; // since this writes to the flash, we do it in the main loop!

// global variables:
	static Pin pinButton = PIN_PUSHBUTTON_1;


// global objects:
	static TRX_Object* trxA=NULL;
	static RF_Object* rf=NULL;



// function declarations:
	static void RfA_ReceiveHandler(RF_Object* rfObj);





// Any Packet was received, forward IRQ to software RF-Module
static void ISR_TrxA_GD0_changed(const Pin* pPin)
{
	// TODO 5: manage forwarding to console
	// DCL_Gd0ChangedHandler(trxA);

	TRACE_DEBUG("GDO0 changed\n");
	if (PIO_Get(&trxA->settings.pins.gd0)) // rising edge
	{
		// do not call the receive handler directly but in main when this flag is set.
		receiveFlag = true;
		receiveTime_100us = time_100us;
		receiveTime_subMicro = AT91C_BASE_SYS->PITC_PIIR & 0xfffff;
		if(receiveTime_100us != time_100us) // if time_100us overflow during this, then do it again
		{
			receiveTime_subMicro = AT91C_BASE_SYS->PITC_PIIR & 0xfffff;
			receiveTime_100us = time_100us;
		}
	}
	else // falling edge
	{

	}
}

static void ISR_Button_changed(const Pin* pPin)
{
	// TODO: manage forwarding to console
}

void RunBoatBox(SETTINGS* set)
{
	DCL_Command cmd;
	printf("Running as Boat-Box (%d)\n\n", set->boardNumber);
	SET_print(set);

	trxA = myalloc(sizeof(TRX_Object));
	rf = myalloc(sizeof(RF_Object));

	TRACE_DEBUG("Configure User-Button...")
	PIO_Configure(&pinButton, 1);
	PIO_ConfigureIt(&pinButton, ISR_Button_changed);
	PIO_EnableIt(&pinButton);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure TRX Module B (433MHz)...");
	TRX_constructor(trxA, TRX_PINSET_A, TRX_CONFIG_CC2500);
	TRX_init(trxA);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Init Console...");
	DCL_Init(trxA, NULL);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Configure GDO0-Interrupt...");
	PIO_ConfigureIt(&(trxA->settings.pins.gd0), ISR_TrxA_GD0_changed);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Setup RF-Communication...");
	RF_Setup(rf, NUM_OF_SLOTS_2G4, TICS_PER_SLOT_2G4, trxA, set->address_2G4,
			RfA_ReceiveHandler, TRANSMIT_DELAY_100us_2G4, TRANSMIT_DELAY_thirdMicroSec_2G4);
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Enable Interrupt for GDO0...");
	PIO_EnableIt(&(trxA->settings.pins.gd0));
	TRACE_DEBUG_WP("[OK]\n");

	TRACE_DEBUG("Setup Photo Detection...");
	PD_Setup();
	TRACE_DEBUG_WP("[OK]\n");

	TIME_T tTrigger = TIME_INVALID;
//	TIME_T tOfSyncFlagIsMissing = TIME_INVALID;
//	bool syncLost = false;
	bool isRunning = true; // can be stopped with command-line
	uint8_t triggeredAtStationID=0xff;

	ALIVE_PACKET packet =
	{
		.batteryVoltageTS = 0xffff,
		.triggerTimeTS_L = TIME_INVALID,
		.triggerTimeTS_R = TIME_INVALID,
		.boatBoxID = set->boardNumber,
		.batteryVoltageBB = 0xffff,
		.triggerTimeBB = TIME_INVALID, // send the last trigger time
		.binRssiBB = 0,
		.stationTriggeredAt=0
	};

	while(1) // endless
	{
		KICK_DOG;
		if(isRunning)
		{
			while(pitFlag == false); // wait for changed time_100us
			pitFlag = false;
			if(RF_Process(rf))
			{
				LED_Toggle(LED_DS_TxA);
				// Update battery voltages:
				UpdateVoltages();
			}

			if(saveSettings){
				saveSettings = false;
				PD_saveSettings();
			}

			if(receiveFlag) // there must not be a second IRQ triggered until this packet is handled,
				// because otherwise the receiveTimes aren't right any more.
			{
				TRACE_DEBUG("Packet Received\n");
				receiveFlag = false;
				if(RF_ReceiveLater(rf, receiveTime_100us, receiveTime_subMicro))
				{
					LED_Toggle(LED_DS_RxA);
				}
			}

			if(time_100us > tTrigger+IGNORE_TRIGGER_DURATION_BB_100us)
			{
				// PD is disabled automatically after trigger so we have to enable it again
				PD_setEnable(true);
				TIME_T tS_temp = PD_Process(POWER_LED_PERIOD_START, false);
				TIME_T tG1_temp = PD_Process(POWER_LED_PERIOD_GOAL1, false);
				TIME_T tG2_temp = PD_Process(POWER_LED_PERIOD_GOAL2, true); // last run = true!
				if(tS_temp != 0) // at start
				{
					tTrigger = tS_temp;
					triggeredAtStationID = 1;
	//				LED_Set(LED_DS1);
				}
				if(tG1_temp != 0) // at goal, internal LED
				{
					tTrigger = tG1_temp;
					triggeredAtStationID = 2;
	//				LED_Set(LED_DS1);
				}
				if(tG2_temp != 0) // at goal, external LED
				{
					tTrigger = tG2_temp;
					triggeredAtStationID = 3;
	//				LED_Set(LED_DS1);
				}
			}

			if(time_100us > tTrigger+2000) // clear LED 0.2 seconds after triggering
			{
				TRACE_DEBUG("Turn LED off\n");
//				LED_Clear(LED_DS1);
			}

			if(rf->synchronized && rf->fifoFilled == false)
			{
				// prepare packet:
				packet.batteryVoltageTS = 0xffff;
				packet.triggerTimeTS_L = TIME_INVALID;
				packet.triggerTimeTS_R = TIME_INVALID;
				packet.boatBoxID = set->boardNumber;
				packet.batteryVoltageBB = vMin*1000; // decimal fixed point: 1234 = 1.234V;
				packet.triggerTimeBB = tTrigger;
				packet.stationTriggeredAt = triggeredAtStationID;

				static int responseQueueIndex=0;
				packet.responseType = PD_responseTypeQueue[responseQueueIndex];
				packet.responseData = PD_responseDataQueue[responseQueueIndex];
				responseQueueIndex = (responseQueueIndex+1)%PD_ResponseQueueSize;


				static bool toggle = true; // first send to MASTER_0
				if(toggle) // alternate the destination address (Master0/1)
				{
					toggle = false;
					RF_LoadFifo(rf, RF_ADDR_MASTER_0, (uint8_t*)&packet, sizeof(ALIVE_PACKET));
				}else
				{
					toggle = true;
					RF_LoadFifo(rf, RF_ADDR_MASTER_1, (uint8_t*)&packet, sizeof(ALIVE_PACKET));
				}
			}

			if(rf->synchronized)
			{
				LED_Set(LED_DS1);
			}else
			{
				LED_Clear(LED_DS1);
			}

		} // end if(isRunning)

		if(DBGUPDC_IsRxReady()) // if Rx has bytes to be read
		{
			cmd = DCL_FeedWithChar(DBGUPDC_GetChar(NULL));
			if(cmd == DCL_cStop)
			{
				isRunning = false;
			}else if( cmd == DCL_cStart)
			{
				isRunning = true;
			}else if(cmd == DCL_cPrintrf)
			{
				RF_Print(rf);
			}else if(cmd == DCL_cPrintpf)
			{
				PD_PrintSettings();
				PD_PrintBuffer();
			}else if(cmd == DCL_cVolt)
			{
				UpdateVoltages();
				PrintVoltages();
			}
		}
	}// end endless loop
}

// Data Packet was Received for this node
// this handler will be called by the software RF-Module
// do something with the data
static void RfA_ReceiveHandler(RF_Object* rfObj)
{
	TRACE_DEBUG("Data Packet Received, len = %d\n", rfObj->rxLength);
	// here is space for handling commands in the Boat-Box

	COMMAND_PACKET* packetInBuffer;
	COMMAND_PACKET p;

	packetInBuffer = (COMMAND_PACKET*)(rfObj->rxBuffer+3); // skip LEN + SOURCE + DESTINATION
	p = *packetInBuffer; // copy packet to local memory, so the rxBuffer can be used again

	printf("got a packet\n");

	if(p.packetType == PACKET_TYPE_COMMAND)
	{
		printf("got a command packet\n");
		printf("  %d,%d,%d\n",p.cmdReceiver, p.command, p.commandData);

		if(((p.cmdReceiver&0xFF00) == 0x100) && // if it is for the 2.4GHz devices AND
			(p.cmdReceiver&0x00FF) == rfObj->address) // it is for me
		{
			// command is for this device!
			switch(p.command)
			{
				case RCmd_RESET:
					printf("RESET by command\n");
					while(1); // wait for death (Watchdog Timeout)
					break;

				case RCmd_ENABLE_SYNCHRONOUS_PULSE_MASK: // data=0: disabled, data!=0: enabled
					PD_setSynchronousPulseMask(p.commandData);
					break;
				case RCmd_SET_PULSE_MEMORY_LENGTH: // data is the length in nr. of pulses stored in the ringbuffer (default=100)
					PD_setPulseMemoryLength(p.commandData);
					break;
				case RCmd_SET_PULSE_DETECTION_LIMIT: // data: the number of pulses that must match the time grid (default = 10)
					PD_setPulseDetectionLimit(p.commandData);
					break;
				case RCmd_SET_MISSING_PULSE_TOLERANCE: // data: number of pulses which must not fall onto the time grid within one sequence of pulses on the time grid (default = 2)
					PD_setMissingPulseTolerance(p.commandData);
					break;
				case RCmd_SET_PULSE_JITTER_TOLERANCE: // data: micro seconds around the nominal time grid. (20us is default, this means +-20us, 40us in total)
					PD_setPulseJitterTolerance(p.commandData);
					break;
				case RCmd_SAVE_PULSE_SETTINGS: // data: micro seconds around the nominal time grid. (20us is default, this means +-20us, 40us in total)
					saveSettings = true;
					// TODO 1: handle save settings in main-loop, not in the ISR
					break;
				case RCmd_LOAD_PULSE_SETTINGS: // data: micro seconds around the nominal time grid. (20us is default, this means +-20us, 40us in total)
					PD_loadSettings();
					break;

				case RCmd_INVALID:
					// nothing to do here!
					break;
			}
		}
	}
}


