/*
 * photodetect.h
 *
 *  Created on: 10.07.2012
 *      Author: fritz
 */

#ifndef PHOTODETECT_H_
#define PHOTODETECT_H_

#include <stdint.h>

void PD_Setup();
void PD_setEnable(bool enable);
TIME_T PD_Process(uint16_t ledPeriod, bool last);

// limit constraints are imlemented in these functions
void PD_setSynchronousPulseMask(uint16_t value);
void PD_setPulseMemoryLength(uint16_t value);
void PD_setPulseDetectionLimit(uint16_t value);
void PD_setMissingPulseTolerance(uint16_t value);
void PD_setPulseJitterTolerance(uint16_t value);
void PD_saveSettings();
void PD_loadSettings();

void PD_PrintSettings();
void PD_PrintBuffer();


#define PD_ResponseQueueSize 7 // this value must not be even! The response packckets are sent alternating to Master0 and Master1
extern uint8_t PD_responseTypeQueue[PD_ResponseQueueSize];
extern uint16_t PD_responseDataQueue[PD_ResponseQueueSize];


#endif /* PHOTODETECT_H_ */
