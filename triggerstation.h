/*
 * triggerstation.h
 *
 *  Created on: 06.07.2012
 *      Author: karl
 */

#ifndef TRIGGERSTATION_H_
#define TRIGGERSTATION_H_

#include "settings.h"

void RunTriggerStation(SETTINGS* set);

#endif /* BOATBOX_H_ */
