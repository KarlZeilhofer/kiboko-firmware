/*
 * myalloc.c
 *
 *  Created on: 06.07.2012
 *      Author: karl
 */

#include "myalloc.h"
#include <stdint.h>
#include <utility/trace.h>


#define MEM_SIZE (1<<14)  // size of total memory reserved

uint8_t mem[MEM_SIZE]={0}; // memory array
uint32_t i = 0;


// TODO: 4-byte alignment!
void* myalloc(size_t numOfBytes)
{
	if(i+numOfBytes<MEM_SIZE)
	{
		void* p = (void*)(mem+i);
		i += numOfBytes;
		TRACE_DEBUG("myalloc() has still %d bytes free\n", (int)(MEM_SIZE-i));
		return p;
	}else
	{
		TRACE_FATAL("ERROR: Out of Memory in %s\n", __FILE__);
		// todo: call exception handler
		return NULL;
	}
}
