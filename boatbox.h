/*
 * boatbox.h
 *
 *  Created on: 06.07.2012
 *      Author: karl
 */

#ifndef BOATBOX_H_
#define BOATBOX_H_

#include "settings.h"

void RunBoatBox(SETTINGS* set);

#endif /* BOATBOX_H_ */
