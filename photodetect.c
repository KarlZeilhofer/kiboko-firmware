
/*
 * This program detects the pulses that are emitted by the LED at the Trigger-Stations
 */

#include "defs.h"
#include "rfswshared.h"
#include "photodetect.h"
#include <stdlib.h>

#include <memories/flash/flashd.h>
#include <efc/efc.h>
#include "settings.h"
#include <string.h>

#define MAX_BUF_SIZE			100		// number of last pulses stored

// TODO 1: load and save values from and to settings
static volatile int syncPulseMaskJitterTol_us = DEFAULT_SYNC_PULSE_MASK_JITTER_TOL;				// if 0, mask is disabled, max. = 50us
static volatile int pulseMemoryLength	=    DEFAULT_PULSE_MEMORY_LENGTH;		// min. = 10,	max. = MAX_BUF_SIZE
static volatile int pulseDetectionLimit	=	 DEFAULT_PULSE_DETECTION_LIMIT;		// min. = 5,	max. = pulseMemoryLength-1
static volatile int missingPulseTolerance =	 DEFAULT_MISSING_PULSE_TOLERANCE;	// min. = 0,	max. = pulseDetectionLimit/2
static volatile int pulseJitterTolerance_us =	 DEFAULT_PULSE_JITTER_TOLERANCE;	// min. = 1us,	max. = 20us
static volatile uint16_t pulseSettingsSaved = 1;

static Pin pinTRIG = PIN_TRIG;							// the pin used to detect the TRIG-signal from the photoreceiver

volatile static TIME_T lastPulse;						// the time of the last received pulse (in 100us)
volatile static uint64_t pulseBuffer[MAX_BUF_SIZE+1];			// a ring-buffer for the last view detected pulses (in 1us steps)
volatile static uint8_t bufIndex=0;						// index of the last element inserted in the ring-buffer

volatile static bool pulse_flag=false;					// this flag is set when a new pulse is detected
volatile static bool enabled;							// enables the detection of new pulses

// prototypes
void ISR_TRIG_changed(const Pin* pPin);
static void reset();
static void setResponseData();


uint8_t PD_responseTypeQueue[PD_ResponseQueueSize] =
	{
		RResp_STATUS_SYNCHRONOUS_PULSE_MASK,
		RResp_PULSE_MEMORY_LENGTH,
		RResp_PULSE_DETECTION_LIMIT,
		RResp_MISSING_PULSE_DETECTION_TOLERANCE,
		RResp_PULSE_JITTER_TOLERANCE,
		RResp_PULSE_SETTINGS_SAVED,
		RResp_INVALID // dummy to have uneven number of entriess
	};
#define SIZE_IN_FLASH (sizeof(uint16_t)*5) // in bytes, we dont save/restore e.g. the saved-flag
uint16_t PD_responseDataQueue[PD_ResponseQueueSize] =
	{0};


void PD_Setup()
{

	TRACE_DEBUG("Configure TRIG-input\n");
	PIO_Configure(&pinTRIG, 1);
	PIO_ConfigureIt(&pinTRIG, ISR_TRIG_changed);
	PIO_EnableIt(&pinTRIG);

	PD_loadSettings();

	reset();
}

void reset()
{
	for(int i=0; i<MAX_BUF_SIZE+1; i++)
	{
		pulseBuffer[i] = 0;
	}

	lastPulse = 0;
	bufIndex = 0;
	pulse_flag = false;
	enabled=true;

	setResponseData();
}

void setResponseData()
{
	PD_responseDataQueue[0] = syncPulseMaskJitterTol_us;
	PD_responseDataQueue[1] = pulseMemoryLength;
	PD_responseDataQueue[2] = pulseDetectionLimit;
	PD_responseDataQueue[3] = missingPulseTolerance;
	PD_responseDataQueue[4] = pulseJitterTolerance_us;
	PD_responseDataQueue[5] = pulseSettingsSaved;
}


void PD_setEnable(bool enable)
{
	enabled=enable;
	if(enabled==false)
	{
		pulse_flag=false;
	}
}


void ISR_TRIG_changed(const Pin* pPin)
{
	if(enabled)
	{
		extern unsigned int pinStatusDump;

		//if (PIO_Get(&pinTRIG)) // rising edge (this is too slow for short pulses of less than 6us)
		if(pinStatusDump & pinTRIG.mask)
		{

			/*
			 * We have to calculate a more accurate time in 1us steps for this. So we take the system time
			 * and convert it to 1us, then we add the current value of the PIT which is based on 1/3us
			 */
			lastPulse=time_100us;
			uint32_t piir = AT91C_BASE_SYS->PITC_PIIR & 0xfffff;
			if(time_100us != lastPulse) // then once again
			{
				piir = AT91C_BASE_SYS->PITC_PIIR & 0xfffff;
				lastPulse=time_100us;
			}
			uint64_t time_us = (uint64_t)lastPulse*100 + piir / 3;

			if(syncPulseMaskJitterTol_us == 0){
				bufIndex=(bufIndex+1)%pulseMemoryLength;		// go to next buffer-element
				pulseBuffer[bufIndex]=time_us;
				pulse_flag=true;
			}else{
				// TODO 2: test this
				static const uint32_t period_us = 9*10*11*100; // smallest common multiple of 900us, 1000us, 1100us. // TODO 5: replace with macro

				time_us-=25; // we have by default a delay of about 25us (22...30us). Lets correct the time by this before doing the compley math.

				uint32_t remainder = (time_us+syncPulseMaskJitterTol_us)%period_us;

				// TODO 5: replace with macro
				uint32_t a = remainder%900;
				uint32_t b = remainder%1000;
				uint32_t c = remainder%1100;

				// TODO 2: increase jitter tolerance, when last synchronization has been a long time ago (out of RF range).
				if( (a<=2*syncPulseMaskJitterTol_us) ||
					(b<=2*syncPulseMaskJitterTol_us) ||
					(c<=2*syncPulseMaskJitterTol_us) )
				{
					bufIndex=(bufIndex+1)%pulseMemoryLength;		// go to next buffer-element
					pulseBuffer[bufIndex]=time_us;
					pulse_flag=true;
				}
			}
		}
		else // falling edge
		{

		}
	}
}


/*
 * call this function during in the main loop
 *
 * ledPeriod		... period of power-LED flashes in us
 * last				... set this flag when this is the last detection task
 * 						# with this feature you can detect several ledPeriods at once
 * 						# at the last function call you have to set the 'last' flag
 *						# if this is the last call, the pulse_flag gets cleard.
 * return value		... detection time or 0 if there was no detection
 */

// Runtime Complexity: O(n) where n is BUF_SIZE
TIME_T PD_Process(uint16_t ledPeriod, bool last)
{
	if(pulse_flag)	// there is one or more new new pulses in the buffer
	{
		if(last)
		{
			pulse_flag=false;
		}
		/*
		 * Now we analyze the last view pulses to find out if this is a valid detection
		 */

		uint8_t missing=0;		// number of missing pulses

		// start with the current pulse
		uint8_t i=bufIndex; // atomic access!
		uint8_t tempBufIndex = i;

		// TODO 2: repeat, if multiple new pulses are in buffer since last call!

		// calculate the expected time of the previous pulse
		uint64_t expectedTime=pulseBuffer[tempBufIndex]-ledPeriod;
		/*
		 * the previous pulse is expected to be in the interval: expectedTime-LED_TOL ... expectedTime+LED_TOL
		 */

		/*
		 * based on the last pulse look back by 1,2,3,...LOOKBACK pulse-periods and try to find matching pulses
		 * if there are not more then MAX_MISSING pulses missing this detection seems to be valid.
		 */
		for(uint8_t lookback=1; lookback<pulseDetectionLimit; lookback++) // the first pulse is also counted
		{
			// step to the previous pulse as long it is after the expected interval
			do
			{
				// step to the previous pulse in the ring-buffer
				if(i==0)
				{
					i=pulseMemoryLength-1;
				}
				else
				{
					i--;
				}

				if(i==tempBufIndex)
				{
					// we are at the beginning of the ring-buffer again, there are to many wrong pulses
					return 0;
				}
			}while(pulseBuffer[i]>expectedTime+2*pulseJitterTolerance_us);
			// now we are before the upper interval-limit
			// --> t[i] <= t_exp+2*jitter

			// so we have to check if we are after the lower limit
			if(pulseBuffer[i]>=expectedTime-2*pulseJitterTolerance_us)
			{
				// --> t[i] >= t_exp-2*jitter

				// this pulse is inside the tolerance
				expectedTime -= ledPeriod;
			}
			else
			{
				// this pulse is missing
				missing++;
				// calculate the expected time of the previous pulse based on the expected time of this pulse
				expectedTime-=ledPeriod;
			}

			if(missing>missingPulseTolerance)
			{
				// there are to many missing pulses
				return 0;
			}
		} // end for

		// this detection is valid, disable new pulses from now on
		PD_setEnable(false);
		// now we calculate the begin-time of this detection
		return lastPulse - (pulseDetectionLimit*ledPeriod)/100;
	}
	else
	{
		return 0;
	}
}


void PD_setSynchronousPulseMask(uint16_t value)
{
	if(value > 50)
		value = 50;


	if(syncPulseMaskJitterTol_us != value)
	{
		syncPulseMaskJitterTol_us = value;
		pulseSettingsSaved = 0;
		setResponseData();
	}
}

void PD_setPulseMemoryLength(uint16_t value)
{
	if(value < 2)
		value = 2;
	if(value > MAX_BUF_SIZE)
		value = MAX_BUF_SIZE;


	if(pulseMemoryLength != value)
	{
		pulseMemoryLength = value;
		pulseSettingsSaved = 0;
		setResponseData();
		reset();
	}
}

void PD_setPulseDetectionLimit(uint16_t value)
{
	if(value < 1)
		value = 1;
	if(value > pulseMemoryLength-1)
		value = pulseMemoryLength-1;

	if(pulseDetectionLimit != value)
	{
		pulseDetectionLimit = value;
		pulseSettingsSaved = 0;
		setResponseData();
	}
}

void PD_setMissingPulseTolerance(uint16_t value)
{
	// at least one pulse must match!
	if(value > (pulseDetectionLimit-1))
		value = pulseDetectionLimit-1;

	if(missingPulseTolerance != value)
	{
		missingPulseTolerance = value;
		pulseSettingsSaved = 0;
		setResponseData();
	}
}

void PD_setPulseJitterTolerance(uint16_t value)
{
	if(value < 1)
		value = 1;
	if(value > 50)
		value = 50;

	if(pulseJitterTolerance_us != value)
	{
		pulseJitterTolerance_us = value;
		pulseSettingsSaved = 0;
		setResponseData();
	}
}

void PD_saveSettings()
{
	// back up interrupt mask:
	uint32_t i = AT91C_BASE_AIC->AIC_IMR; // interrupt mask register
	AT91C_BASE_AIC->AIC_IDCR = 0xffffffff; // disable all interrupts
	if(!FLASHD_Write(PULSE_SETTINGS_FLASH_ADDR, (void*)PD_responseDataQueue, SIZE_IN_FLASH))
	{
		TRACE_INFO_WP("    [OK]\n");
		TRACE_INFO_WP("    Check Settings in RAM against FLASH...");
		if(memcmp((void*)PD_responseDataQueue, (void*)PULSE_SETTINGS_FLASH_ADDR, SIZE_IN_FLASH)==0)
		{
			TRACE_INFO_WP("[OK]");
		}else
		{
			TRACE_ERROR("\n Error on comparing Settings in RAM to FLASH (in %s, Line %d)", __FILE__, __LINE__);
		}
	}
	else
	{
		TRACE_ERROR("\nError on writing set to flash (in %s, Line %d)\n", __FILE__, __LINE__);
	}

	// restore interrupt mask
	AT91C_BASE_AIC->AIC_IECR = i;

	pulseSettingsSaved = 1;
	setResponseData();
}

void PD_loadSettings()
{
	memcpy((void*)PD_responseDataQueue, (void*)PULSE_SETTINGS_FLASH_ADDR, SIZE_IN_FLASH);

	// check limits

	// synchronousPulseDetection
	if(PD_responseDataQueue[0] > 50){
		PD_responseDataQueue[0] = 50;
	}

	// pulseMemoryLength
	if(PD_responseDataQueue[1] > MAX_BUF_SIZE){
		PD_responseDataQueue[1] = MAX_BUF_SIZE;
	}

	// pulseDetectionLimit
	if(PD_responseDataQueue[2] > MAX_BUF_SIZE){
		PD_responseDataQueue[2] = MAX_BUF_SIZE;
	}
	if(PD_responseDataQueue[2] < 1){
		PD_responseDataQueue[2] = 1;
	}

	// missingPulseTolerance
	if(PD_responseDataQueue[3] > PD_responseDataQueue[2]-1){
		PD_responseDataQueue[3] = PD_responseDataQueue[2]-1;
	}

	// pulseJitterTolerance
	if(PD_responseDataQueue[4] > 50){
		PD_responseDataQueue[4] = 50;
	}

	syncPulseMaskJitterTol_us = PD_responseDataQueue[0];
	pulseMemoryLength		  = PD_responseDataQueue[1];
	pulseDetectionLimit		  = PD_responseDataQueue[2];
	missingPulseTolerance	  = PD_responseDataQueue[3];
	pulseJitterTolerance_us	  = PD_responseDataQueue[4];

	pulseSettingsSaved = 1;
	setResponseData();
}

void PD_PrintSettings()
{
	printf("Current settings of photo detector filter settings:\n");
	printf("  synchronousPulseDetection: %u\n",		(unsigned int)syncPulseMaskJitterTol_us);
	printf("  pulseMemoryLength: %u\n",				(unsigned int)pulseMemoryLength);
	printf("  pulseDetectionLimit: %u\n",			(unsigned int)pulseDetectionLimit);
	printf("  missingPulseTolerance: %u\n",			(unsigned int)missingPulseTolerance);
	printf("  pulseJitterTolerance: %u\n",			(unsigned int)pulseJitterTolerance_us);
}

void PD_PrintBuffer()
{
	uint8_t i=bufIndex; // atomic access!
	uint8_t tempBufIndex = i;

	for(uint8_t lookback=0; lookback<pulseMemoryLength; lookback++)
	{
		printf("  t[%2d]=%d.%02d", lookback, (int)(pulseBuffer[i]/100), (int)(pulseBuffer[i]%100));
		if(lookback>0)
		{
			int32_t diff = pulseBuffer[(i+1)%pulseMemoryLength] - pulseBuffer[i];
			printf(" dt[%2d]=%d.%02d", lookback, (int)(diff/100), (int)(diff%100));
		}
		printf("\n");


		// step to the previous pulse in the ring-buffer
		if(i==0)
		{
			i=pulseMemoryLength-1;
		}
		else
		{
			i--;
		}

		if(i==tempBufIndex)
		{
			// we are at the beginning of the ring-buffer again, there are to many wrong pulses
			break;
		}
	}
}
