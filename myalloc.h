/*
 * myalloc.h
 *
 *  Created on: 06.07.2012
 *      Author: karl
 */

#ifndef MYALLOC_H_
#define MYALLOC_H_

#include <stdlib.h>

// returns a pointer to a block of memory, which is initialized with zeros
void* myalloc(size_t numOfBytes);

// for simplicity, no free() is implemented.

#endif /* MYALLOC_H_ */
