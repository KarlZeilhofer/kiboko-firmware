/*
 * globals.h
 *
 *  Created on: 07.06.2012
 *      Author: fritz
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

/*
 * this header defines some global variables like system time etc.
 */

typedef uint32_t TIME_T;
extern volatile TIME_T time_100us; // global system time in 100-micro seconds



#endif /* GLOBALS_H_ */
