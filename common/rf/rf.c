/*
 * rf.c
 *
 *  Created on: 07.06.2012
 *      Author: fritz
 */

#include <utility/trace.h>

#include <trx/trx.h>
#include <globals.h>
#include <rf/rf.h>
#include <utility/led.h>
#include <dcl/dcl.h>
#include <pit/pit.h>

#include "../softpll.h"

#define TIME_TOL_100us	10		// in time-tics; (1ms) if time-difference is larger than this it will be hard-updated
#define PIT_INTERVAL (300) // PIT runs with MCK/16, which gives @48MHz and 10kHz a Value of 300
#define INVALID_TIME (0xffff0000U)

static void flushTxBuffer(RF_Object* self);
static void flushRxBuffer(RF_Object* self);

// self function is defined in rfswshared.c
extern void PrintTime(TIME_T t); // print a human readable time, without a new-line


/*
 * measured time delay between:
 * # sending the TX-command-strobe
 * 	and
 * # receiving a sync-packet
 *
 * at: SPI=600kHz, RF=250kbps, TRACE=INFO --> delay = 958us
 * at: SPI=9MHz, RF=250kbps, TRACE=INFO --> delay = 727us
 *
 */

/*
 * this is a kind of constructor for RF_Object
 * sets up the interrupt-service-routine for the external interrupt pin
 */
void RF_Setup(RF_Object* self, uint8_t timeSlots, uint16_t ticsPerSlot,
			TRX_Object* trx, uint8_t address, void (*rxHandler)(RF_Object* self),
			uint16_t txDelay_100us, uint16_t txDelay_thirdMicroSec)
{
	TRACE_DEBUG("RF_Setup()...");

	self->timeSlots=timeSlots;
	self->ticsPerSlot=ticsPerSlot;
	self->ticsPerPeriod=self->timeSlots*self->ticsPerSlot;
	self->trx=trx;
	self->address=address;

	self->rxHandler=rxHandler;

	self->fifoFilled=false;
	self->synchronized=false;
	self->syncPacket=false;
	self->nextTxTime = INVALID_TIME; // set to invalid time
	self->lastTxTime = INVALID_TIME;

	self->txDelay_100us = txDelay_100us;
	self->txDelay_thirdMicroSec = txDelay_thirdMicroSec;

	TRX_writeCommandStrobe(trx, TRX_CMD_SFRX, 0);	// flush rx buffer, before going to Rx-State
	TRX_writeCommandStrobe(trx, TRX_CMD_SFTX, 0);	// flush tx buffer, before going to Rx-State
	TRX_writeCommandStrobe(trx, TRX_CMD_SRX, 0);		// set by default to receive mode!

	/* HERE A NOP COMMAND STROBE IS NEEDED BEFORE READING THE STATUS BYTE */
	// --> use do-while instead of while in the TRX_waitForState()

	TRACE_DEBUG_WP("    Wait for Rx-State...");
	TRX_waitForState(self->trx, TRX_ST_RX);		// wait for trx-module to get in receive-mode

	TRACE_DEBUG("[OK]\n");
}


/*
 * Load FIFO buffer with data and get ready to send
 * this function returns 0 on success, and -1 when an error occured
 * maximal data length: 59 Bytes
 */
int RF_LoadFifo(RF_Object* self, uint8_t destAddress, uint8_t* data, uint8_t length)
{
	if(!self->fifoFilled && self->synchronized)
	{
		if(TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES) != 0)
		{
			TRACE_ERROR("#### RX-FIFO is not empty #### in %s:%s():%d\n", __FILE__, __func__, __LINE__);

			flushTxBuffer(self);

			return -1;
		}

//#if TRACE_LEVEL >= TRACE_LEVEL_DEBUG
//		bytes = TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES);
//		TRACE_DEBUG("RF_LoadFifo after flush: fifo is filled with %u bytes\n", bytes);
//#endif

		/*
		 * packet format (for sending):
		 *
		 * bytes:	1		1			1			N
		 * 			len		srcAddr		destAddr	data
		 */

		// 1st byte: length
		TRX_writeFifo(self->trx, length+2);

		// 2nd byte: source address
		TRX_writeFifo(self->trx, self->address);

		// 3rd byte: destination address
		TRX_writeFifo(self->trx, destAddress);

		// data
		for(uint8_t i=0; i<length; i++)
		{
			TRX_writeFifo(self->trx, data[i]);
		}

#if TRACE_LEVEL >= TRACE_LEVEL_DEBUG
		uint8_t bytes = TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES);
		TRACE_DEBUG("data loaded to FIFO (now it is filled with %u bytes)\n", bytes);
#endif

		/*
		 *  calculate the time when packet has to be sent
		 *  the times for this module are:
		 *
		 *  sendTime = address * ticsPerSlot + ticsPerPeriod * k	where k is a natural number
		 *
		 *  now we have to find k so that the next available slot is choosen:
		 *
		 *  k = (time - address * ticsPerSlot) / ticsPerPeriod - 1		(integer division!)
		 */

		//																		 +*** interger division!
		uint32_t transmitTime = (((time_100us - self->address*self->ticsPerSlot) / self->ticsPerPeriod) + 1) * self->ticsPerPeriod + self->address * self->ticsPerSlot;

		self->syncPacket=false;
		self->nextTxTime=transmitTime;
		self->fifoFilled=true;

		return 0;
	}
	else
	{
		TRACE_WARNING("the TX-FIFO is not empty or we are not synchronized, ignore new data-bytes\n");
		return -1;
	}
}


/*
 * Load FIFO buffer with current time to synchronize the other modules to this one
 * this function returns the time when the RF_Transmit function has to be called
 */
int RF_LoadSync(RF_Object* self)
{
	if(!self->fifoFilled)
	{
		TRACE_DEBUG("RF_LoadSync() called, time_100us = %u\n", (unsigned int)time_100us);

		uint8_t bytes = TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES); // number of bytes in TX-Fifo
		if(bytes!=0)
		{
			TRACE_ERROR("#### TX-FIFO is not empty ####\n");

			flushTxBuffer(self);
		}

		// when the main-program calls this function it is assumed that we are synchronized (e.g. this is the timebase-module)
		self->synchronized = true;

		self->lastSyncTime = time_100us;
		//									 	+*** add some time to fill fifo		 +*** integer division!
		uint32_t transmitTime = (((time_100us + 1 - self->address*self->ticsPerSlot) / self->ticsPerPeriod) + 1) * self->ticsPerPeriod + self->address * self->ticsPerSlot;

		// 1. byte: length
		TRX_writeFifo(self->trx, 6);

		// 2. byte: source address
		TRX_writeFifo(self->trx, self->address);

		// 3. byte: destination address
		TRX_writeFifo(self->trx, RF_ADDR_BROADCAST);

		TRACE_INFO("    transmitTime = %u, Line %d\n", (unsigned int)transmitTime,  __LINE__);

		// 4, 5, 6, 7. byte: time
		uint8_t* timeMem = (uint8_t*)&transmitTime;
		TRX_writeFifo(self->trx, timeMem[0]);
		TRX_writeFifo(self->trx, timeMem[1]);
		TRX_writeFifo(self->trx, timeMem[2]);
		TRX_writeFifo(self->trx, timeMem[3]);

//#if TRACE_LEVEL >= TRACE_LEVEL_DEBUG
//		uint8_t bytes = TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES);
//		TRACE_DEBUG("loaded sync to FIFO (now it is filled with %u bytes)\n", bytes);
//#endif

		self->syncPacket=true;
		self->nextTxTime=transmitTime;
		TRACE_DEBUG("    nextTxTime = %u\n", (unsigned int)transmitTime);
		self->fifoFilled=true;

		return 0;
	}
	else
	{
		// we should never be here.
		TRACE_WARNING("the TX-FIFO is not empty, ignore new sync-data\n");
		return -1;
	}
}


/*
 * call this during the main loop!
 * when this function is called AND the fifo is filled AND the time has come,
 * the content of the FIFO buffer is sent
 *
 * NOTE: Es kommt unter gewissen bedingungen dazu, dass Rx-FIFO und Tx-FIFO nicht
 * vollständig geleert werden. In RF_Process() wird das aber nicht erkannt, da
 * das Statusbyte nicht aktuell ist.
 * In Folge setzt dann die Synchronisation aus.
 * Wo der Fehler ist, ist noch nicht klar.
 *
 * returns true, if a packet was sent, otherwise false
 */
bool RF_Process(RF_Object* self)
{
	TRACE_DEBUG("RF_Process()...\n");
	bool ret = false;

	// check number of bytes in the Rx-FIFO.
	// 		there shouldn't be more than 40 to 50 bytes, because no packet has this size!
	TRACE_DEBUG_WP("    check Rx-Fifo-Bytes\n")
	uint8_t rxBytes = TRX_readStatusRegister(self->trx, TRX_SR_RXBYTES);
	if((rxBytes&0x7F) >= 50)
	{
		TRACE_INFO("   There are too many (>=50) bytes in the receive FIFO. There is the risk of Rx-Overflow\n");
		// todo: to something against this
	}

	if((rxBytes&0x7F) > 64)		// under certain conditions the RF-chip can get stuck in RX-mode when the RF-FIFO overflows
	{
		TRACE_ERROR("RX-FIFO is stuck\n");
		TRX_writeCommandStrobe(self->trx, TRX_CMD_SIDLE, 0);
		TRX_waitForState(self->trx, TRX_ST_IDLE);		// wait for trx-module to get in IDLE-mode
		TRX_writeCommandStrobe(self->trx, TRX_CMD_SFRX, 0);
	}

	// check trx-module-state
	// if there is an error go back to receive mode anyway
	TRACE_DEBUG_WP("    getValidStatusByte()\n");
	TRX_getValidStatusByte(self->trx);
	TRACE_DEBUG_WP("    Check Status Byte\n");
	switch(self->trx->statusByte.sb.state)
	{
		case TRX_ST_IDLE: // this is normal after calibrating, which is done after every Tx-Command automatically
		{
			// IDLE -> go to receive mode
			TRACE_INFO("#### TRX-Module in IDLE-mode ####\n");
			TRX_writeCommandStrobe(self->trx, TRX_CMD_SRX, 0);
			TRX_waitForState(self->trx, TRX_ST_RX);		// wait for trx-module to get in receive-mode

			break;
		}
		case TRX_ST_RXFIFO_OVERFLOW:
		{
			TRACE_WARNING("#### RX-FIFO overflow ####\n");
			TRX_writeCommandStrobe(self->trx, TRX_CMD_SFRX, 0);
			TRX_waitForState(self->trx, TRX_ST_IDLE);		// wait for trx-module to get in IDLE-mode
			// now we are in IDLE state
			TRX_writeCommandStrobe(self->trx, TRX_CMD_SRX, 0);
			TRX_waitForState(self->trx, TRX_ST_RX);		// wait for trx-module to get in receive-mode
			break;
		}
		case TRX_ST_TXFIFO_UNDERFLOW:
		{
			TRACE_WARNING("#### TX-FIFO underflow ####\n");
			TRX_writeCommandStrobe(self->trx, TRX_CMD_SFTX, 0);
			TRX_waitForState(self->trx, TRX_ST_IDLE);		// wait for trx-module to get in IDLE-mode
			// now we are in IDLE state
			TRX_writeCommandStrobe(self->trx, TRX_CMD_SRX, 0);
			TRX_waitForState(self->trx, TRX_ST_RX);		// wait for trx-module to get in receive-mode

			break;
		}
		default:
		{
			break;
		}
	}


	TRACE_DEBUG_WP("    Check time-out of sync\n");
	// check for time-out of sync
	// todo: calculate limit of out of sync delay
	if(self->synchronized && time_100us-self->lastSyncTime > RF_N_PERIODS_SYNC_TIMEOUT*self->ticsPerPeriod)
	{
		// if there is no synchronisation for more than 10 periods the time is invalid
		self->synchronized=false;
		TRACE_INFO("######### Sync Timed Out ############\n");
	}


	// check for data to be sent
	if(self->synchronized)
	{
		TRACE_DEBUG_WP("    Send Data:\n");
		// send data
		if(self->fifoFilled && (time_100us+1) >= self->nextTxTime)
		{
			TRACE_DEBUG_WP("    Check Time:\n");
			if((time_100us+1) == self->nextTxTime) // TODO 1: limit synchronization to sub-100us!!
				// the transmit time is very critical! Allow a max. of 100us latency
			{
				// send transmit command strobe

				// NOTE: dieser Befehl sollte mit so wenig wie möglich Jitter gegenüber der lokalen
				// systemzeit (time_100us) gesendet werden, damit sich der Empfänger auf die mitgeteilte
				// zeit auch verlassen kann.

				// die Zeit die seit dem letztn PIT-ISR vergangen ist, ist an dieser Stelle nicht konstant
				// daher warten wir auf die flanke von time_100us
				while(time_100us != self->nextTxTime){
					AT91C_BASE_WDTC->WDTC_WDCR = 0xA5000001; //	KICK_DOG
				}

				while((AT91C_BASE_SYS->PITC_PIIR & 0xfffff) < 3*24); // Künstlicher zusatzoffset, um die 10us-Jitter-Problematik zu umgehen.

				TRX_writeCommandStrobe(self->trx, TRX_CMD_STX, 0);
				self->nextTxTime=INVALID_TIME;
				TRACE_DEBUG("Sent Tx CmdStrobe. time_100us = %u\n", (unsigned int)time_100us);
				ret = true;
				self->lastTxTime = time_100us;
			}
			else	// we are too late to send this packet
			{
				TRACE_DEBUG_WP("    We are too late\n");
				if(self->syncPacket)
				{
					// if it is a sync we have to flush the TX-FIFO
					TRACE_ERROR("#### time-sync-packet outdated ####\n");

					flushTxBuffer(self);
				}
				else // data: try to transmit this packet in the next timeslot
				{
					TRACE_DEBUG("    Move Packet to next time slot\n");
					//																		 +*** integer division!
					self->nextTxTime = (((time_100us - self->address*self->ticsPerSlot) / self->ticsPerPeriod) + 1) * self->ticsPerPeriod + self->address * self->ticsPerSlot;
				}
			}
		}


	}

	// check for packets with tx-time too far in the future (more than 5 periods)
	if(self->fifoFilled && self->nextTxTime != INVALID_TIME && self->nextTxTime > time_100us+5*self->ticsPerPeriod)
	{
		// flush self packet
		TRACE_ERROR("packet too far in the future\n");
		flushTxBuffer(self);
	}

	// time to check for empty buffer
	//   the tx-fifo is write-blocked for the complete transmit-time-slot
// FUNKTIONIERT NICHT! siehe Fehlerbild 2
//	if(self->fifoFilled && (time_100us > self->lastTxTime + self->ticsPerSlot))
//	{
//		if(TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES) == 0)
//		{
//			self->fifoFilled = false; // fifo-filled gets cleared only here!
//			self->lastTxTime = INVALID_TIME;
//		}
//		else
//		{
//			TRACE_ERROR("### TX-Command-Strobe left a filled Tx-Buffer - flush it now ####\n");
//			flushTxBuffer(self);
//		}
//	}


	// get number of TX-FIFO bytes
	if(TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES) == 0)
	{
		// FIFO is empty
		self->fifoFilled=false;
		self->lastTxTime=INVALID_TIME;
	}

	if(self->fifoFilled && time_100us > self->lastTxTime + self->ticsPerSlot)
	{
		TRACE_ERROR("### TX-Command-Strobe left a filled Tx-Buffer - flush it now ####\n");
		flushTxBuffer(self);
	}

	TRACE_DEBUG_WP("End of RF_Process\n");
	return ret; // true, when packet was sent.
}

/*
 * this function is called by the interrupt handler from the main program
 * does the synchronization, if a sync-packet was received
 * else call the rxHandler() in the main program
 */
bool RF_Receive(RF_Object* self) // not time critical receive
{
	return RF_ReceiveLater(self, time_100us, 0);
}

// returns true when a packet was received successfully
bool RF_ReceiveLater(RF_Object* self, TIME_T receivedTime_100us, uint32_t receivedTime_thirdMircoSec) // time critical receive!
{
	//TRACE_DEBUG("RF_Receive() is called\n");
	bool ret=false;



	/***	read data from RX-FIFO		***/

	// check if RX-FIFO is filled
	if(TRX_readStatusRegister(self->trx, TRX_SR_RXBYTES) == 0)
	{
		TRACE_WARNING("receive called but RX-FIFO is empty\n");
		// this could happen after CRC-autoflush
		return false;
	}

//	// check CRC
//	if(!(TRX_readStatusRegister(self->trx,TRX_SR_PKTSTATUS) & (1<<7))) // CRC_OK is bit 7 in PKTSTATUS
//	{
//		TRACE_WARNING("CRC is invalid - flushing Rx-FIFO now\n");
//		flushRxBuffer(self);
//		return false;
//	}

	/*
	 * packet format (for receiving):
	 *
	 * bytes:	1		1			1			N		1		1
	 * 			len		srcAddr		destAddr	data	RSSI	LQI
	 */
	// read 1. byte: length of data
	uint8_t len=TRX_readFifo(self->trx);

	// check packet length
	if((TRX_readStatusRegister(self->trx, TRX_SR_RXBYTES)&0x7F) < len+2) // N-Data + RSSI + LQI
	{
		TRACE_ERROR("There are too few bytes in the RX-FIFO\n");
		flushRxBuffer(self);
		return false;
	}

	// there are enough bytes in the RX-FIFO, continue reading

	self->rxLength=len+3; // length-byte + N-Data + RSSI + LQI
		// N-Data= src,des,data


	// read 2. byte: source address
	uint8_t src=TRX_readFifo(self->trx);

	// read 3. byte: destination address
	uint8_t dest=TRX_readFifo(self->trx);

	if(dest==self->address || dest==RF_ADDR_BROADCAST  || 					// only for me, or for everyone
			(self->address==RF_ADDR_MASTER_0 && dest==RF_ADDR_MASTER_1) ||	// for Master_1 but Master_0 also receives
			(self->address==RF_ADDR_MASTER_1 && dest==RF_ADDR_MASTER_0))	// for Master_0 but Master_1 also receives
	{
		ret = true;
		// self packet is for me
		self->rxBuffer[0]=len;
		self->rxBuffer[1]=src;
		self->rxBuffer[2]=dest;

		for(uint8_t i=3; i<self->rxLength; i++)
		{
			// read all the other bytes
			self->rxBuffer[i]=TRX_readFifo(self->trx);
		}
	}
	else
	{
		// self is not for me -> flush the rest of the packet
		for(uint8_t i=3; i<self->rxLength; i++)
		{
			TRX_readFifo(self->trx);
		}
	}



	/***	clean up RX-FIFO	***/

	// check if RX-FIFO is empty, otherwise flush it
	if(TRX_readStatusRegister(self->trx, TRX_SR_RXBYTES) != 0)
	{
		TRACE_ERROR("#### RX-FIFO is not empty after readout #### in %s:%s():%d\n", __FILE__, __func__, __LINE__);
		flushRxBuffer(self);
	}



	/***	interpret packet header	***/

	// check destination address byte:
	if(dest==self->address ||
			(self->address==RF_ADDR_MASTER_0 && dest==RF_ADDR_MASTER_1) ||	// for Master_1 but Master_0 also receives
			(self->address==RF_ADDR_MASTER_1 && dest==RF_ADDR_MASTER_0))	// for Master_0 but Master_1 also receives
	{
		TRACE_DEBUG("Packet is addressed to me\n");
		// this packet fits to my address
		// call handler function in the main programm
		self->rxHandler(self);
	}
	else if(dest==RF_ADDR_BROADCAST && self->address!=RF_ADDR_MASTER_0 && self->address!=RF_ADDR_MASTER_1)
		// broadcast packet from master is always a sync packet
	{
		// this is a timesync packet

		// save time of this point of time, so the additional time delay between receiving the packet
		// and calling the RF_Receive(), so this delay can be compensated for the syncronization.
		TIME_T timeNow_100us = time_100us;
		uint32_t timeNow_thirdMicroSec = AT91C_BASE_SYS->PITC_PIIR;
		if(timeNow_100us != time_100us)
		{
			timeNow_100us = time_100us;
			timeNow_thirdMicroSec = AT91C_BASE_SYS->PITC_PIIR;
		}

		int32_t extraDelay_thirdMicroSec = (int32_t)timeNow_thirdMicroSec - receivedTime_thirdMircoSec;
		TIME_T extraDelay_100us = timeNow_100us - receivedTime_100us;
		if(extraDelay_thirdMicroSec < 0)
		{
			extraDelay_thirdMicroSec += PIT_INTERVAL; // PIT-Tag
			extraDelay_100us--;
		}

		// the packet needs time to be transmitted so we have to add this time to compensate the error.
		uint32_t newTime_thirdMicroSec = self->txDelay_thirdMicroSec + extraDelay_thirdMicroSec;
		uint32_t newTime_100us = *((uint32_t*)(&(self->rxBuffer[3]))) + self->txDelay_100us + extraDelay_100us;
		if(newTime_thirdMicroSec >= PIT_INTERVAL)
		{
			newTime_thirdMicroSec -= PIT_INTERVAL;
			newTime_100us++;
		}
#if TRACE_LEVEL == TRACE_LEVEL_DEBUG
		uint32_t oldTime = time_100us; // save current time, for debug output after sync
#endif

		//LED_Set(LED_DS1);
		//LED_Clear(LED_DS1);


		self->lastSyncTime=time_100us;
		if(newTime_100us>time_100us+TIME_TOL_100us || newTime_100us<time_100us-TIME_TOL_100us)
		{
			// the time-difference is too big, make a hard update
			time_100us=newTime_100us;

			TRACE_DEBUG("Got Time Sync Packet\n");
			TRACE_DEBUG("    Make hard sync\n");
		}
		else
		{
			/*
			 * smooth update via Software-PLL
			 *
			 */

			int32_t deltaT_thirdMicroSec =
					((int64_t)timeNow_100us*300 + timeNow_thirdMicroSec) -
					((int64_t)newTime_100us*300 + newTime_thirdMicroSec);

			PIT_DisableIT(); // TODO 2: try without disabling interrupt
			SoftPLL_updateFrequencyOffset(deltaT_thirdMicroSec);
			PIT_EnableIT();

			// +-10us
			if(deltaT_thirdMicroSec > -30 && deltaT_thirdMicroSec < 30)
			{
				self->synchronized = true;
				TRACE_INFO("    Synchronized, time=%u, Line %d\n", (unsigned int)self->lastSyncTime,  __LINE__);
			}
		}


		TRACE_DEBUG("    newTime_100us = %u\n", (unsigned int)newTime_100us);
		TRACE_DEBUG("    time difference newTime_100us-oldTime=%d\n", (int)newTime_100us - (int)oldTime);
	}
	else
	{
		TRACE_DEBUG("    Packet is neither addressed to me nor is it a broadcast\n");
	}

	// make all the debug output after syncing!
	TRACE_DEBUG("    Packet Info:\n");
	TRACE_DEBUG("    len = %d    src = %d    dest = %d\n", len, src, dest);
	TRACE_INFO("    RSSI = %dcBm\n", (int)DCL_num2rssi(self->rxBuffer[self->rxLength-2]));

	return ret;
}


/*
 *
typedef struct _RF_Object
{
	uint8_t timeSlots;			// number of time slots (number of peers) >2 (address=0, 1 are the masters)
	uint16_t ticsPerSlot;		// time for one slot in 100us steps
	uint32_t ticsPerPeriod;		// time of one period in 100us steps
	TRX_Object* trx;			// transceiver object (CC2500 or CC1101)
	uint8_t address;			// address of this platform

	void (*rxHandler)(struct _RF_Object* self);		// receive handler

	bool fifoFilled;		// is the TX FIFO filled

	uint8_t rxBuffer[64];	// buffer for received data
	uint8_t rxLength;		// length of received data

	uint32_t nextTxTime; 	// time when the next packet has to be sent

	bool synchronized;		// is this unit synchroniced to the time-base
	uint32_t lastSyncTime;	// time of last synchronisation

	bool syncPacket;		// is this a sync packet

	uint16_t txDelay_100us; // transmission delay, needed for time compensation
	uint16_t txDelay_thirdMicroSec; // transmission delay, needed for time compensation
}RF_Object;
 *
 */

void RF_Print(RF_Object* self)
{
	printf("RF-Module: \n");
	printf("    TRX-Module: %s\n", self->trx->name);
	printf("    timeSlots: %d\n", (int)(self->timeSlots));			// number of time slots (number of peers) >2 (address=0, 1 are the masters)
	printf("    ticsPerSlot: %d\n", (int)(self->ticsPerSlot));		// time for one slot in 100us steps
	printf("    ticsPerPeriod: %d\n", (int)(self->ticsPerPeriod));		// time of one period in 100us steps
	printf("    address: %d\n", (int)(self->address));			// address of self platform

	printf("    rxHandler: 0x%08X\n", (unsigned int)(self->rxHandler));		// receive handler

	printf("    fifoFilled: %d\n", (int)(self->fifoFilled));		// is the TX FIFO filled

	//printf(": %\n", self->rxBuffer[64];	// buffer for received data
	printf("    rxLength: %d\n", (int)(self->rxLength));		// length of received data

	printf("    nextTxTime: "); PrintTime(self->nextTxTime); printf(" (%u)\n", (unsigned int)(self->nextTxTime)); 	// time when the next packet has to be sent
	printf("    lasTxTime: "); PrintTime(self->lastTxTime); printf(" (%u)\n", (unsigned int)(self->lastTxTime)); 		// time when the last TX-CMD-STROBE was sent

	printf("    synchronized: %d\n", (int)(self->synchronized));		// is self unit synchroniced to the time-base
	printf("    lastSyncTime: "); PrintTime(self->lastSyncTime); printf(" (%u)\n", (unsigned int)(self->lastSyncTime));	// time of last synchronisation

	printf("    syncPacket: %d\n", (int)(self->syncPacket));		// is self a sync packet

	printf("    txDelay_100us: %d\n", (int)(self->txDelay_100us)); // transmission delay, needed for time compensation
	printf("    txDelay_thirdMicroSec: %d\n", (int)(self->txDelay_thirdMicroSec)); // transmission delay, needed for time compensation
}

static void flushTxBuffer(RF_Object* self)
{
	TRX_writeCommandStrobe(self->trx, TRX_CMD_SIDLE,0);	// go to idle
	TRX_waitForState(self->trx, TRX_ST_IDLE);				// wait for module to get in IDLE mode
	TRX_writeCommandStrobe(self->trx, TRX_CMD_SFTX,0); 	// flush tx fifo
	TRX_writeCommandStrobe(self->trx, TRX_CMD_SRX,0); 	// go to Rx-State
	TRX_waitForState(self->trx, TRX_ST_RX);				// wait for module to get in RX mode

	self->nextTxTime = INVALID_TIME;

	if(TRX_readStatusRegister(self->trx, TRX_SR_TXBYTES) == 0)
	{
		//self->fifoFilled = false; // flag gets cleared only in RF_Process()!!!
	}else
	{
		TRACE_ERROR("### TX-Fifo not empty after flushing ###\n");
	}
}


static void flushRxBuffer(RF_Object* self)
{
	 /*
	  * we cannot use the SFRX command because we are in receive-mode
	  * so we simply do dummy-reads until the buffer is empty
	  */
	while((TRX_readStatusRegister(self->trx, TRX_SR_RXBYTES)&0x7F) > 0)		// there are still bytes in the RX-FIFO
	{
		TRX_readFifo(self->trx);
	}
}
