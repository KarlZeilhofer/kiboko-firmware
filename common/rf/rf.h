/*
 * This module implements time slot communication over RF-modules from Texas Instruments.
 *
 * rf.h
 *
 *  Created on: 07.06.2012
 *      Author: fritz
 */

#ifndef RF_H_
#define RF_H_

#include <stdint.h>
#include <stdbool.h>
#include <trx/trx.h>

typedef uint32_t TIME_T;

#define RF_ADDR_MASTER_0 (0)
#define RF_ADDR_MASTER_1 (1)
#define RF_ADDR_INVALID (254)
#define RF_ADDR_BROADCAST (255) // broadcast address (used for time synchronisation)

#define RF_N_PERIODS_SYNC_TIMEOUT (100) // kann theoretisch auf 123 erhöht werden. (mit 30ppm Quarz, 2ms Slottime, 650us Nachrichtendauer, max. 375us abweichung, und 22 Teilnehmer)

/*
 * packet format (for receiving):
 *
 * bytes:	1		1			1			N		1		1
 * 			len		srcAddr		destAddr	data	RSSI	LQI
 */


typedef struct _RF_Object
{
	uint8_t timeSlots;			// number of time slots (number of peers) >2 (address=0, 1 are the masters)
	uint16_t ticsPerSlot;		// time for one slot in 100us steps
	uint32_t ticsPerPeriod;		// time of one period in 100us steps
	TRX_Object* trx;			// transceiver object (CC2500 or CC1101)
	uint8_t address;			// address of this platform

	void (*rxHandler)(struct _RF_Object* self);		// receive handler

	bool fifoFilled;		// true if the TX-FIFO contains (usefull or useless) data. If this is false one can load new data.

	uint8_t rxBuffer[70];	// buffer for received data (RX-FIFO has 64-bytes but we want to be sure)
	uint8_t rxLength;		// length of received data

	uint32_t nextTxTime; 	// time when the next packet has to be sent or INVALID_TIME when there is nothing to be sent.
	uint32_t lastTxTime;	// time of last TX-Command-Strobe in RF_Process() or INVALID_TIME if the TX-FIFO was completely sent.
								// used to check for empty buffer afterwards.

	bool synchronized;		// is this unit synchroniced to the time-base
	uint32_t lastSyncTime;	// time of last synchronisation

	bool syncPacket;		// is this a sync packet

	uint16_t txDelay_100us; // transmission delay, needed for time compensation
	uint16_t txDelay_thirdMicroSec; // transmission delay, needed for time compensation
}RF_Object;


/*
 * this is a kind of constructor for RF_Object
 * sets up the interrupt-service-routine for the external interrupt pin
 */
void RF_Setup(RF_Object* obj, uint8_t timeSlots, uint16_t ticsPerSlot,
			TRX_Object* trx, uint8_t address, void (*rxHandler)(RF_Object* obj),
			uint16_t txDelay_100us, uint16_t txDelay_thirdMicroSec);


/*
 * Load FIFO buffer with data and get ready to send
 * maximal data length: 59 Bytes
 */
int RF_LoadFifo(RF_Object* self, uint8_t destAddress, uint8_t* data, uint8_t length);


/*
 * Load FIFO buffer with current time to synchronize the other modules to this one
 * This function also sets the synchronized flag
 */
int RF_LoadSync(RF_Object* self);


/*
 * call this function periodically in the PIT IRQ
 * it triggers the transmit of a prepared fifo buffer
 */
bool RF_Process(RF_Object* self);


/*
 * this function is called by the interrupt handler
 */
bool RF_Receive(RF_Object* self);
bool RF_ReceiveLater(RF_Object* self, TIME_T t_100us, uint32_t t_thirdMircoSec);

/*
 * Print all informations about self RF-Module-Object
 */
void RF_Print(RF_Object* obj);




#endif /* RF_H_ */
