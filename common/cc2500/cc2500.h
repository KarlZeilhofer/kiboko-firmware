// Header File for the Texas Instruments CC2500 Transceiver Chip
// Karl Zeilhofer
// 8.5.2012

/*
 * The CC2500 is controlled via the Serial Peripheral Interface (SPI).
 * Where the transceiver is the slave and the micro controller is the master.
 * All transfers are with most significant bit (MSB) first.
 * Each message has a header byte.
 *
 */

#ifndef __CC2500_H
#define __CC2500_H

#include <stdint.h>
#include <board.h>
#include <pio/pio.h>


/********** Configuration Register Definitions ************************************************************************/

// Configuration Register Addresses:
typedef enum{
    CC2500_CR_IOCFG2 = 0x0000, // GDO2Output Pin Configuration
    CC2500_CR_IOCFG1 = 0x0001, // GDO1Output Pin Configuration
    CC2500_CR_IOCFG0 = 0x0002, // GDO0Output Pin Configuration
    CC2500_CR_FIFOTHR = 0x0003, // RX FIFO and TX FIFO Thresholds
    CC2500_CR_SYNC1 = 0x0004, // Sync Word, High Byte
    CC2500_CR_SYNC0 = 0x0005, // Sync Word, Low Byte
    CC2500_CR_PKTLEN = 0x0006, // Packet Length
    CC2500_CR_PKTCTRL1 = 0x0007, // Packet Automation Control
    CC2500_CR_PKTCTRL0 = 0x0008, // Packet Automation Control
    CC2500_CR_ADDR = 0x0009, // Device Address
    CC2500_CR_CHANNR = 0x000A, // Channel Number
    CC2500_CR_FSCTRL1 = 0x000B, // Frequency Synthesizer Control
    CC2500_CR_FSCTRL0 = 0x000C, // Frequency Synthesizer Control
    CC2500_CR_FREQ2 = 0x000D, // Frequency Control Word, High Byte
    CC2500_CR_FREQ1 = 0x000E, // Frequency Control Word, Middle Byte
    CC2500_CR_FREQ0 = 0x000F, // Frequency Control Word, Low Byte
    CC2500_CR_MDMCFG4 = 0x0010, // Modem Configuration
    CC2500_CR_MDMCFG3 = 0x0011, // Modem Configuration
    CC2500_CR_MDMCFG2 = 0x0012, // Modem Configuration
    CC2500_CR_MDMCFG1 = 0x0013, // Modem Configuration
    CC2500_CR_MDMCFG0 = 0x0014, // Modem Configuration
    CC2500_CR_DEVIATN = 0x0015, // Modem Deviation Setting
    CC2500_CR_MCSM2 = 0x0016, // Main Radio Control State Machine Configuration
    CC2500_CR_MCSM1 = 0x0017, // Main Radio Control State Machine Configuration
    CC2500_CR_MCSM0 = 0x0018, // Main Radio Control State Machine Configuration
    CC2500_CR_FOCCFG = 0x0019, // Frequency Offset Compensation Configuration
    CC2500_CR_BSCFG = 0x001A, // Bit Synchronization Configuration
    CC2500_CR_AGCCTRL2 = 0x001B, // AGC Control
    CC2500_CR_AGCCTRL1 = 0x001C, // AGC Control
    CC2500_CR_AGCCTRL0 = 0x001D, // AGC Control
    CC2500_CR_WOREVT1 = 0x001E, // High Byte Event0 Timeout
    CC2500_CR_WOREVT0 = 0x001F, // Low Byte Event0 Timeout
    CC2500_CR_WORCTRL = 0x0020, // Wake On Radio Control
    CC2500_CR_FREND1 = 0x0021, // Front End RX Configuration
    CC2500_CR_FREND0 = 0x0022, // Front End TX configuration
    CC2500_CR_FSCAL3 = 0x0023, // Frequency Synthesizer Calibration
    CC2500_CR_FSCAL2 = 0x0024, // Frequency Synthesizer Calibration
    CC2500_CR_FSCAL1 = 0x0025, // Frequency Synthesizer Calibration
    CC2500_CR_FSCAL0 = 0x0026, // Frequency Synthesizer Calibration
    CC2500_CR_RCCTRL1 = 0x0027, // RC Oscillator Configuration
    CC2500_CR_RCCTRL0 = 0x0028, // RC Oscillator Configuration
    CC2500_CR_FSTEST = 0x0029, // Frequency Synthesizer Calibration Control
    CC2500_CR_PTEST = 0x002A, // Production Test
    CC2500_CR_AGCTEST = 0x002B, // AGC Test
    CC2500_CR_TEST2 = 0x002C, // Various Test Settings
    CC2500_CR_TEST1 = 0x002D, // Various Test Settings
    CC2500_CR_TEST0 = 0x002E // Various Test Settings
}CC2500_CONFIGURATION_REGISTER;

#define CC2500_NUM_CR (0x2F) // Number of Configuration Registers

/************** GDOx Signal Selection (x = 0, 1 or 2)**********************************************************************
	GDOx_CFG[5:0]] Description
	0 (0x00) Associated to the RX FIFO: Asserts when RX FIFO is filled at or above the RX FIFO threshold. De-asserts when RX FIFO is drained below the same threshold.
	1 (0x01) Associated to the RX FIFO: Asserts when RX FIFO is filled at or above the RX FIFO threshold or the end of packet is reached. De-asserts when the RX FIFO is empty.
	2 (0x02) Associated to the TX FIFO: Asserts when the TX FIFO is filled at or above the TX FIFO threshold. De-asserts when the TX FIFO is below the same threshold.
	3 (0x03) Associated to the TX FIFO: Asserts when TX FIFO is full. De-asserts when the TX FIFO is drained below theTX FIFO threshold.
	4 (0x04) Asserts when the RX FIFO has overflowed. De-asserts when the FIFO has been flushed.
	5 (0x05) Asserts when the TX FIFO has underflowed. De-asserts when the FIFO is flushed.
	6 (0x06) Asserts when sync word has been sent / received, and de-asserts at the end of the packet. In RX, the pin will de-assert when the optional address check fails or the RX FIFO overflows. In TX the pin will de-assert if the TX FIFO underflows.
	7 (0x07) Asserts when a packet has been received with CRC OK. De-asserts when the first byte is read from the RX FIFO. Only valid if PKTCTRL0.CC2400_EN=1.
	8 (0x08) Preamble Quality Reached. Asserts when the PQI is above the programmed PQT value.
	9 (0x09) Clear channel assessment. High when RSSI level is below threshold (dependent on the current CCA_MODE setting)
	10 (0x0A) Lock detector output. The PLL is in lock if the lock detector output has a positive transition or is constantly logic high. To check for PLL lock the lock detector output should be used as an interrupt for the MCU.
	11 (0x0B) Serial Clock. Synchronous to the data in synchronous serial mode.
				In RX mode, data is set up on the falling edge by CC2500 when GDOx_INV=0.
				In TX mode, data is sampled by CC2500 on the rising edge of the serial clock when GDOx_INV=0.
	12 (0x0C) Serial Synchronous Data Output (DO). Used for synchronous serial mode.
	13 (0x0D) Serial Data Output. Used for asynchronous serial mode.
	14 (0x0E) Carrier sense. High if RSSI level is above threshold.
	15 (0x0F) CRC_OK. The last CRC comparison matched. Cleared when entering/restarting RX mode. Only valid if PKTCTRL0.CC2400_EN=1.
	16 (0x10) to 21 (0x15) Reserved � used for test.
	22 (0x16) RX_HARD_DATA[1]. Can be used together with RX_SYMBOL_TICK for alternative serial RX output.
	23 (0x17) RX_HARD_DATA[0]. Can be used together with RX_SYMBOL_TICK for alternative serial RX output.
	24 (0x18) Reserved � used for test.
	25 (0x19) Reserved � used for test.
	26 (0x1A) Reserved � used for test.
	27 (0x1B) PA_PD. Note: PA_PD will have the same signal level in SLEEP and TX states. To control an external PA or RX/TX switch in applications where the SLEEP state is used it is recommended to use GDOx_CFGx=0x2F instead.
	28 (0x1C) LNA_PD. Note: LNA_PD will have the same signal level in SLEEP and RX states. To control an external LNA or RX/TX switch in applications where the SLEEP state is used it is recommended to use GDOx_CFGx=0x2F instead.
	29 (0x1D) RX_SYMBOL_TICK. Can be used together with RX_HARD_DATA for alternative serial RX output.
	30 (0x1E) to 35 (0x23) Reserved � used for test.
	36 (0x24) WOR_EVNT0
	37 (0x25) WOR_EVNT1
	38 (0x26) Reserved � used for test.
	39 (0x27) CLK_32k
	40 (0x28) Reserved � used for test.
	41 (0x29) CHIP_RDYn
	42 (0x2A) Reserved � used for test.
	43 (0x2B) XOSC_STABLE
	44 (0x2C) Reserved � used for test.
	45 (0x2D) GDO0_Z_EN_N. When this output is 0, GDO0 is configured as input (for serial TX data).
	46 (0x2E) High impedance (3-state)
	47 (0x2F) HW to 0 (HW1 achieved by setting GDOx_INV=1). Can be used to control an external LNA/PA or RX/TX switch.
	48 (0x30) CLK_XOSC/1
	49 (0x31) CLK_XOSC/1.5
	50 (0x32) CLK_XOSC/2
	51 (0x33) CLK_XOSC/3
	52 (0x34) CLK_XOSC/4
	53 (0x35) CLK_XOSC/6
	54 (0x36) CLK_XOSC/8
	55 (0x37) CLK_XOSC/12
	56 (0x38) CLK_XOSC/16
	57 (0x39) CLK_XOSC/24
	58 (0x3A) CLK_XOSC/32
	59 (0x3B) CLK_XOSC/48
	60 (0x3C) CLK_XOSC/64
	61 (0x3D) CLK_XOSC/96
	62 (0x3E) CLK_XOSC/128
	63 (0x3F) CLK_XOSC/192
*/


/*************** Status Register Definitions  *************************************************************************/
typedef enum{
    CC2500_SR_PARTNUM = 0x0030, // Chip ID
    CC2500_SR_VERSION = 0x0031, // Chip Version
    CC2500_SR_FREQEST = 0x0032, // Frequency Offset Estimate from Demodulator
    CC2500_SR_LQI = 0x0033, // Demodulator Estimate for Link Quality
    CC2500_SR_RSSI = 0x0034, // Received Signal Strength Indication
    CC2500_SR_MARCSTATE = 0x0035, // Main Radio Control State Machine State
    CC2500_SR_WORTIME1 = 0x0036, // High Byte of WOR Time
    CC2500_SR_WORTIME0 = 0x0037, // Low Byte of WOR Time
    CC2500_SR_PKTSTATUS = 0x0038, // Current GDOxStatus and Packet Status
    CC2500_SR_VCO_VC_DAC = 0x0039, // Current Setting from PLL Calibration Module
    CC2500_SR_TXBYTES = 0x003A, // Underflow and Number of Bytes
    CC2500_SR_RXBYTES = 0x003B, // Underflow and Number of Bytes
    CC2500_SR_RCCTRL1_STATUS = 0x003C, // Last RC Oscillator Calibration Result
    CC2500_SR_RCCTRL0_STATUS = 0x003D // Last RC Oscillator Calibration Result
}CC2500_STATUS_REGISTER;


/************ Command Strobe Definitions ******************************************************************************
 * Address Strobe Name Description
	0x3F	FIFO		Write to Fifo-Buffer (needs data byte)
*/
typedef enum  {
	CC2500_CMD_SRES=0x30, 	// Reset chip.
	CC2500_CMD_SFSTXON = 0x31, 	// Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1). If in RX (with CCA):
							//  Go to a wait state where only the synthesizer is running (for quick RX / TX turnaround).
	CC2500_CMD_SXOFF= 0x32, 		// Turn off crystal oscillator.
	CC2500_CMD_SCAL= 0x33, 		// Calibrate frequency synthesizer and turn it off. SCAL can be strobed from IDLE mode without
							//  setting manual calibration mode (MCSM0.FS_AUTOCAL=0)
	CC2500_CMD_SRX= 0x34,			// Enable RX. Perform calibration first if coming from IDLE and MCSM0.FS_AUTOCAL=1.
	CC2500_CMD_STX= 0x35,			// In IDLE state: Enable TX. Perform calibration first if MCSM0.FS_AUTOCAL=1.
							// If in RX state and CCA is enabled: Only go to TX if channel is clear.
	CC2500_CMD_SIDLE= 0x36,		// Exit RX / TX, turn off frequency synthesizer and exit Wake-On-Radio mode if applicable.
	CC2500_CMD_SWOR_=0x38,	// Start automatic RX polling sequence (Wake-on-Radio) as described in Section 19.5 if
							//  WORCTRL.RC_PD=0.
	CC2500_CMD_SPWD= 0x39,		// Enter power down mode when CSn goes high.
	CC2500_CMD_SFRX= 0x3A,		// Flush the RX FIFO buffer. Only issue SFRX in IDLE or RXFIFO_OVERFLOW states.
	CC2500_CMD_SFTX= 0x3B,		// Flush the TX FIFO buffer. Only issue SFTX in IDLE or TXFIFO_UNDERFLOW states.
	CC2500_CMD_SWORRST= 0x3C,		// Reset real time clock to Event1 value.
	CC2500_CMD_SNOP= 0x3D			// No operation. May be used to get access to the chip status byte.
}CC2500_COMMAND_STROBE;

typedef struct sCC2500_Pins
{
	// refer to the order, defined in board.h
	Pin spiMISO;
	Pin spiMOSI;
	Pin spiSCK;
	Pin spiNPCS;
	Pin gd0;
	Pin gd2;
}CC2500_Pins;

typedef struct sCC2500_Settings
{
	AT91PS_SPI spiBase; // pointer to the SPI base address
	uint8_t spiID; // SPI device ID
	uint8_t npcs; // number of NPCS used (0...3)
	uint32_t spiBitrate; // SPI bit rate in Hz
	CC2500_Pins pins; // used pins (4x SPI, GD0, GD2)
	uint8_t regConfigured[CC2500_NUM_CR]; // not configured = 0; configured = 1
	uint8_t configRegs[CC2500_NUM_CR];
	uint8_t readonly[CC2500_NUM_CR];
}CC2500_Settings;

// instance for one tranceiver chip
typedef struct sCC2500_Object
{
	char name[50]; // used for info output
	uint8_t statusByte; // is updated on every read/write to the SPI
	CC2500_Settings settings;
}CC2500_Object;

// set default settings
CC2500_Object* CC2500_constructor(CC2500_Object* pObj);

// init function for hardware (SPI, pins, interrupts)
CC2500_Object* CC2500_init(CC2500_Object* pObj);

void CC2500_writeConfigurationRegister(CC2500_Object* pObj, CC2500_CONFIGURATION_REGISTER regAddress, uint8_t value);
uint8_t CC2500_readConfigurationRegister(CC2500_Object* pObj, CC2500_CONFIGURATION_REGISTER regAddress);
uint8_t CC2500_readStatusRegister(CC2500_Object* pObj, CC2500_STATUS_REGISTER regAddress);
void CC2500_writeCommandStrobe(CC2500_Object* pObj, CC2500_COMMAND_STROBE regAddress, uint8_t RWnBit);

uint8_t CC2500_readFifo(CC2500_Object* pObj);
void CC2500_writeFifo(CC2500_Object* pObj, uint8_t data);
uint8_t CC2500_readPaTable0(CC2500_Object* pObj);
void CC2500_writePaTable0(CC2500_Object* pObj, uint8_t data);

void CC2500_setupIrqHandlers(CC2500_Object* pObj, void (*ISR_GD0)(const Pin* pPin), void (*ISR_GD2)(const Pin* pPin));
void CC2500_setSettingsReg(CC2500_Object* pObj, uint8_t regAddr, uint8_t regValue);
int8_t CC2500_compareRegisterValues(CC2500_Object* pObj);


#endif
