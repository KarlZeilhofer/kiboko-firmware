// Sorce File for the Texas Instruments CC2500 Transceiver Chip
// Karl Zeilhofer
// 8.5.2012

// this driver currently only supports 16-bit writes/reads to/from the SPI

#include "cc2500.h"
#include <string.h>
#include <spi/spi.h>
#include <pio/pio_it.h>
#include <stdint.h>

#define BURST_BIT ((uint8_t)(1<<6))
#define READ_WRITEn_BIT ((uint8_t)(1<<7))
#define ADDRESS_MASK ((uint8_t)0x3F) // use this for masking a register address

extern volatile uint32_t time_100us;
// prototypes of private functions:
static uint16_t spiWirte(CC2500_Object* self, uint16_t txData);
static void resetChip(CC2500_Object* self);
void wait(uint32_t microSeconds);



// has to be run before using the CC2500 object in other functions.
// default settings
// use RF-Module A
CC2500_Object* CC2500_constructor(CC2500_Object* self)
{
	strcpy(self->name, "RF-Module A (CC2500)");
	self->settings.spiBase = BOARD_SPI_BASE;
	self->settings.spiID = BOARD_SPI_ID;
	self->settings.npcs = BOARD_RF_A_NPCS;
	self->settings.spiBitrate = BOARD_RF_A_SPI_CLK;
	CC2500_Pins pins = {BOARD_RF_A_PINS}; // use pins for RF-Module A
	self->settings.pins = pins;

	// clear arrays:
	for(int n=0; n<CC2500_NUM_CR; n++)
	{
		self->settings.regConfigured[n]=0;
		self->settings.configRegs[n]=0;
		self->settings.readonly[n]=0; // set to writeable by default
	}

	// set some registers to read only, because in the datasheet it
	// is recommendet not to write into this registers
	self->settings.readonly[CC2500_CR_FSTEST]   = 1;
	self->settings.readonly[CC2500_CR_PTEST]    = 1;
	self->settings.readonly[CC2500_CR_AGCTEST]  = 1;


//	// lansers config0:
//	CC2500_setSettingsReg(self, CC2500_CR_IOCFG2, 0x29);//0x29;
////	CC2500_setSettingsReg(self, CC2500_CR_IOCFG1, 0x06);//0x2e;
//	CC2500_setSettingsReg(self, CC2500_CR_IOCFG0, 0x06);//0x2e;//0x06;
////	CC2500_setSettingsReg(self, CC2500_CR_FIFOTHR, //0x07;
////	CC2500_setSettingsReg(self, CC2500_CR_SYNC1, //0xd3;
////	CC2500_setSettingsReg(self, CC2500_CR_SYNC0, //0x91;
//	CC2500_setSettingsReg(self, CC2500_CR_PKTLEN, 0x3D); // 62 = 64 - 2 status bytes
//	CC2500_setSettingsReg(self, CC2500_CR_PKTCTRL1, 0x04); // no address check, append status
//	CC2500_setSettingsReg(self, CC2500_CR_PKTCTRL0, 0x05); // variable length, crc enabled
//	CC2500_setSettingsReg(self, CC2500_CR_ADDR, 0x00);//0x00;
//	CC2500_setSettingsReg(self, CC2500_CR_CHANNR, 0x00);//0x00;
//	CC2500_setSettingsReg(self, CC2500_CR_FSCTRL1, 0x08);//0x0a;
//	CC2500_setSettingsReg(self, CC2500_CR_FSCTRL0, 0x00);//0x00;
//	CC2500_setSettingsReg(self, CC2500_CR_FREQ2, 0x5D);//0x5d;
//	CC2500_setSettingsReg(self, CC2500_CR_FREQ1, 0x93);//0x93;
//	CC2500_setSettingsReg(self, CC2500_CR_FREQ0, 0xB1);//0xb1;
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG4, 0x86);//0x2d;
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG3, 0x83);//0x3b;
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG2, 0x03);//0x73;
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG1, 0x22);//0x22;
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG0, 0xF8);//0xf8;
//	CC2500_setSettingsReg(self, CC2500_CR_DEVIATN, 0x44);//0x00;
////	CC2500_setSettingsReg(self, CC2500_CR_MCSM2]    = //0x07;
////	CC2500_setSettingsReg(self, CC2500_CR_MCSM1]    = //0x30;
//	CC2500_setSettingsReg(self, CC2500_CR_MCSM0, 0x08);//0x18;   // std= 0x18; 0x08 = MCSM0 (never calibrate)
//	CC2500_setSettingsReg(self, CC2500_CR_FOCCFG, 0x16);//0x1d;
//	CC2500_setSettingsReg(self, CC2500_CR_BSCFG, 0x6C);//0x1c;
//	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL2, 0x03);//0xc7;
//	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL1, 0x40);//0x00;
//	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL0, 0x91);//0xb0;
////	CC2500_setSettingsReg(self, CC2500_CR_WOREVT1]  = //0x87;
////	CC2500_setSettingsReg(self, CC2500_CR_WOREVT0]  = //0x6b;
////	CC2500_setSettingsReg(self, CC2500_CR_WORCTRL]  = //0xf8;
//	CC2500_setSettingsReg(self, CC2500_CR_FREND1, 0x56);//0xb6;
//	CC2500_setSettingsReg(self, CC2500_CR_FREND0, 0x10);//0x10;
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL3, 0xA9);//0xea;
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL2, 0x0A);//0x0a;
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL1, 0x00);//0x00;
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL0, 0x11);//0x11;
////	CC2500_setSettingsReg(self, CC2500_CR_RCCTRL1]  = //0x41;
////	CC2500_setSettingsReg(self, CC2500_CR_RCCTRL0]  = //0x00;
//	CC2500_setSettingsReg(self, CC2500_CR_FSTEST, 0x59);//0x59; // read only
////	CC2500_setSettingsReg(self, CC2500_CR_PTEST]    = //0x7f;
////	CC2500_setSettingsReg(self, CC2500_CR_AGCTEST]  = //0x3f;
//	CC2500_setSettingsReg(self, CC2500_CR_TEST2, 0x88);//0x88;
//	CC2500_setSettingsReg(self, CC2500_CR_TEST1, 0x31);//0x31;
//	CC2500_setSettingsReg(self, CC2500_CR_TEST0, 0x0B);//0x0b;



//// 500 kBit/s (RfStudio):
//	// Sync word qualifier mode = 30/32 sync word bits detected
//	// CRC autoflush = false
//	// Channel spacing = 199.951172
//	// Data format = Normal mode
//	// Data rate = 499.878
//	// RX filter BW = 812.500000
//	// Preamble count = 8
//	// Address config = No address check
//	// Whitening = false
//	// Carrier frequency = 2432.999908
//	// Device address = 0
//	// TX power = 0
//	// Manchester enable = true
//	// CRC enable = true
//	// Phase transition time = 0
//	// Modulation format = MSK
//	// Base frequency = 2432.999908
//	// Modulated = true
//	// Channel number = 0
//
//	// Control Registers
//
//	CC2500_setSettingsReg(self, CC2500_CR_IOCFG2,   0x29);
//	CC2500_setSettingsReg(self, CC2500_CR_IOCFG1,   0x2e);
//	CC2500_setSettingsReg(self, CC2500_CR_IOCFG0,   0x06); // assert on finished packet tx/rx
//	CC2500_setSettingsReg(self, CC2500_CR_FIFOTHR,  0x07);
//	CC2500_setSettingsReg(self, CC2500_CR_SYNC1,    0xd3);
//	CC2500_setSettingsReg(self, CC2500_CR_SYNC0,    0x91);
//	CC2500_setSettingsReg(self, CC2500_CR_PKTLEN,   0x3D); // 62 Bytes
//	CC2500_setSettingsReg(self, CC2500_CR_PKTCTRL1, 0x04); // no address check, append status
//	CC2500_setSettingsReg(self, CC2500_CR_PKTCTRL0, 0x05); // variable length, crc enabled
//	CC2500_setSettingsReg(self, CC2500_CR_ADDR,     0x00);
//	CC2500_setSettingsReg(self, CC2500_CR_CHANNR,   0x00);
//	CC2500_setSettingsReg(self, CC2500_CR_FSCTRL1,  0x0c);
//	CC2500_setSettingsReg(self, CC2500_CR_FSCTRL0,  0x00);
//	CC2500_setSettingsReg(self, CC2500_CR_FREQ2,    0x5d);
//	CC2500_setSettingsReg(self, CC2500_CR_FREQ1,    0x93);
//	CC2500_setSettingsReg(self, CC2500_CR_FREQ0,    0xb1);
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG4,  0x0e);
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG3,  0x3b);
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG2,  0x7b);
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG1,  0x42);
//	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG0,  0xf8);
//	CC2500_setSettingsReg(self, CC2500_CR_DEVIATN,  0x00);
//	CC2500_setSettingsReg(self, CC2500_CR_MCSM2,    0x07);
//	CC2500_setSettingsReg(self, CC2500_CR_MCSM1,    0x30);
//	CC2500_setSettingsReg(self, CC2500_CR_MCSM0,    0x18);
//	CC2500_setSettingsReg(self, CC2500_CR_FOCCFG,   0x1d);
//	CC2500_setSettingsReg(self, CC2500_CR_BSCFG,    0x1c);
//	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL2, 0xc7);
//	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL1, 0x40);
//	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL0, 0xb0);
//	CC2500_setSettingsReg(self, CC2500_CR_WOREVT1,  0x87);
//	CC2500_setSettingsReg(self, CC2500_CR_WOREVT0,  0x6b);
//	CC2500_setSettingsReg(self, CC2500_CR_WORCTRL,  0xf8);
//	CC2500_setSettingsReg(self, CC2500_CR_FREND1,   0xb6);
//	CC2500_setSettingsReg(self, CC2500_CR_FREND0,   0x10);
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL3,   0xea);
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL2,   0x0a);
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL1,   0x00);
//	CC2500_setSettingsReg(self, CC2500_CR_FSCAL0,   0x19);
//	CC2500_setSettingsReg(self, CC2500_CR_RCCTRL1,  0x41);
//	CC2500_setSettingsReg(self, CC2500_CR_RCCTRL0,  0x00);
//	CC2500_setSettingsReg(self, CC2500_CR_FSTEST,   0x59);
//	CC2500_setSettingsReg(self, CC2500_CR_PTEST,    0x7f);
//	CC2500_setSettingsReg(self, CC2500_CR_AGCTEST,  0x3f);
//	CC2500_setSettingsReg(self, CC2500_CR_TEST2,    0x88);
//	CC2500_setSettingsReg(self, CC2500_CR_TEST1,    0x31);
//	CC2500_setSettingsReg(self, CC2500_CR_TEST0,    0x0b);


// 250kbit/s (Rf-Studio):
	// Sync word qualifier mode = 30/32 sync word bits detected
	// CRC autoflush = false
	// Channel spacing = 199.951172
	// Data format = Normal mode
	// Data rate = 249.939
	// RX filter BW = 541.666667
	// Preamble count = 4
	// Address config = No address check
	// Whitening = false
	// Carrier frequency = 2432.999908
	// Device address = 0
	// TX power = 0
	// Manchester enable = false
	// CRC enable = true
	// Phase transition time = 0
	// Modulation format = MSK
	// Base frequency = 2432.999908
	// Modulated = true
	// Channel number = 0

	// Control Registers

	CC2500_setSettingsReg(self, CC2500_CR_IOCFG2,   0x29);
	CC2500_setSettingsReg(self, CC2500_CR_IOCFG1,   0x2e);
	CC2500_setSettingsReg(self, CC2500_CR_IOCFG0,   0x06);
	CC2500_setSettingsReg(self, CC2500_CR_FIFOTHR,  0x07);
	CC2500_setSettingsReg(self, CC2500_CR_SYNC1,    0xd3);
	CC2500_setSettingsReg(self, CC2500_CR_SYNC0,    0x91);
	CC2500_setSettingsReg(self, CC2500_CR_PKTLEN,   0xff);
	CC2500_setSettingsReg(self, CC2500_CR_PKTCTRL1, 0x04);
	CC2500_setSettingsReg(self, CC2500_CR_PKTCTRL0, 0x05);
	CC2500_setSettingsReg(self, CC2500_CR_ADDR,     0x00);
	CC2500_setSettingsReg(self, CC2500_CR_CHANNR,   0x00);
	CC2500_setSettingsReg(self, CC2500_CR_FSCTRL1,  0x0a);
	CC2500_setSettingsReg(self, CC2500_CR_FSCTRL0,  0x00);
	CC2500_setSettingsReg(self, CC2500_CR_FREQ2,    0x5d);
	CC2500_setSettingsReg(self, CC2500_CR_FREQ1,    0x93);
	CC2500_setSettingsReg(self, CC2500_CR_FREQ0,    0xb1);
	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG4,  0x2d);
	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG3,  0x3b);
	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG2,  0x73);
	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG1,  0x22);
	CC2500_setSettingsReg(self, CC2500_CR_MDMCFG0,  0xf8);
	CC2500_setSettingsReg(self, CC2500_CR_DEVIATN,  0x00);
	CC2500_setSettingsReg(self, CC2500_CR_MCSM2,    0x07);
	CC2500_setSettingsReg(self, CC2500_CR_MCSM1,    0x30);
	CC2500_setSettingsReg(self, CC2500_CR_MCSM0,    0x18);
	CC2500_setSettingsReg(self, CC2500_CR_FOCCFG,   0x1d);
	CC2500_setSettingsReg(self, CC2500_CR_BSCFG,    0x1c);
	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL2, 0xc7);
	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL1, 0x00);
	CC2500_setSettingsReg(self, CC2500_CR_AGCCTRL0, 0xb0);
	CC2500_setSettingsReg(self, CC2500_CR_WOREVT1,  0x87);
	CC2500_setSettingsReg(self, CC2500_CR_WOREVT0,  0x6b);
	CC2500_setSettingsReg(self, CC2500_CR_WORCTRL,  0xf8);
	CC2500_setSettingsReg(self, CC2500_CR_FREND1,   0xb6);
	CC2500_setSettingsReg(self, CC2500_CR_FREND0,   0x10);
	CC2500_setSettingsReg(self, CC2500_CR_FSCAL3,   0xea);
	CC2500_setSettingsReg(self, CC2500_CR_FSCAL2,   0x0a);
	CC2500_setSettingsReg(self, CC2500_CR_FSCAL1,   0x00);
	CC2500_setSettingsReg(self, CC2500_CR_FSCAL0,   0x11);
	CC2500_setSettingsReg(self, CC2500_CR_RCCTRL1,  0x41);
	CC2500_setSettingsReg(self, CC2500_CR_RCCTRL0,  0x00);
	CC2500_setSettingsReg(self, CC2500_CR_FSTEST,   0x59);
	CC2500_setSettingsReg(self, CC2500_CR_PTEST,    0x7f);
	CC2500_setSettingsReg(self, CC2500_CR_AGCTEST,  0x3f);
	CC2500_setSettingsReg(self, CC2500_CR_TEST2,    0x88);
	CC2500_setSettingsReg(self, CC2500_CR_TEST1,    0x31);
	CC2500_setSettingsReg(self, CC2500_CR_TEST0,    0x0b);


	return self;
}

// reset CC2500 (with pin toggling and software SPI)
// configure pins
// init SPI
//
CC2500_Object* CC2500_init(CC2500_Object* self)
{
	resetChip(self); // Reset CC2500

	// configure pins:
	PIO_Configure(&(self->settings.pins.spiMISO), 1);
	PIO_Configure(&(self->settings.pins.spiMOSI), 1);
	PIO_Configure(&(self->settings.pins.spiSCK), 1);
	PIO_Configure(&(self->settings.pins.spiNPCS), 1);
	PIO_Configure(&(self->settings.pins.gd0), 1);
	PIO_Configure(&(self->settings.pins.gd2), 1);

	uint32_t status = self->settings.spiBase->SPI_SR; // self is needed, because the SR isn't writeable,
		// which is needed for the logical combination!
	if((status & AT91C_SPI_SPIENS) == 0) // when spi isn't enabled (=configured)
	{
		// workaround for SPI_Configure
			// it must not be called twice, because if so, the SPI_ConfigureNPCS of the first
			// configuration gets cleared by the software reset made in SPI_Configure.

		// configure SPI, and enable clock in the PMC:
		SPI_Configure(self->settings.spiBase, self->settings.spiID,
				1 << 0 | // master mode enable
				1 << 1 | // peripheral select (1=variable peripheral)
				0 << 2 | // peripheral chip select decode (0=direct connection)
				1 << 4 | // mode fault detection disable
				0 << 7 | // local loop back enable (0=off)
				0 << 16 | // peripheral chip select [0:3]; (see page 247 in the prelimainary)
				// not used for variable NPCS
				0 << 24); // delay between chip selects (default minimum 6 MCK cycles))
	}

	SPI_ConfigureNPCS(self->settings.spiBase, self->settings.npcs,
			0 << 0 | //clock polarity; // The inactive state value of SPCK is logic level zero.
			1 << 1 | // clock phase; //Data is captured on the leading edge of SPCK and changed on the following edge of SPCK.
			0 << 3 | // chip select remains active after transfer (0=false)
			(16 - 8) << 4 | // bits per transfer (8...16); value = num_bits-8
			BOARD_MCK / self->settings.spiBitrate << 8 | // SPCK bautdrate divider. BR=MCK/value
			0 << 16 | // delay before SPCK (default 1/2 SPCK cycle)
			0 << 24); // delay between consecutive transfers

	SPI_Enable(self->settings.spiBase);

	// it cannot be configured, when it is in power down mode!
//	CC2500_writeCommandStrobe(self, CC2500_CMD_SPWD, 0); // power down
//	wait(100);


	uint8_t n=0;
	for(n=0; n<CC2500_NUM_CR; n++)
	{
		if(self->settings.regConfigured[n] && !self->settings.readonly[n])
		{
			CC2500_writeConfigurationRegister(self, n, self->settings.configRegs[n]);
		}
	}
	CC2500_writePaTable0(self, 0xff); // set to full transmit power = 1dBm

	CC2500_writeCommandStrobe(self, CC2500_CMD_SCAL, 0); // clibrate
	wait(1000);
	return self;
}

// returns the read spi-word
static uint16_t spiWirte(CC2500_Object* self, uint16_t txData)
{
	uint16_t rxData;
	SPI_Write(self->settings.spiBase, self->settings.npcs, txData);
	rxData = SPI_Read(self->settings.spiBase);

	self->statusByte = rxData >> 8; // the high byte of rxData is allways the status byte
	return rxData;
}

// this can be used for reading the FIFO too
uint8_t CC2500_readStatusRegister(CC2500_Object* self, CC2500_STATUS_REGISTER regAddress)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	if(regAddress >= 0x30 && regAddress < 0x3F)
	{
		headerByte = READ_WRITEn_BIT | BURST_BIT; // Burst bit has the meaning of "Read Status Register" here
		headerByte |= regAddress & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	rxData = spiWirte(self, txData);
	return (rxData & 0xFF); // mask lower byte
}


// set read bit to get raimaining bytes in the Rx-FIFO
// clear read-bit to get free bytes in the Tx-FIFO
void CC2500_writeCommandStrobe(CC2500_Object* self, CC2500_COMMAND_STROBE regAddress, uint8_t RWnBit)
{
	uint8_t headerByte=0;
	uint16_t txData=0;
	uint8_t regAddr = regAddress; // typecast from int to uint8

	if(regAddr >= 0x30 && regAddr < 0x3F)
	{
		if(RWnBit)
		{
			headerByte = READ_WRITEn_BIT; // set read bit, no burst bit
			// not setting Burst bit has the meaning of "Write Command Strobe" here
			// setting the read-bit has the meaning of getting the RX-FIFO bytes remaining
		}
		headerByte |= regAddr & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	spiWirte(self, txData);
}

void CC2500_writeConfigurationRegister(CC2500_Object* self, CC2500_CONFIGURATION_REGISTER regAddress, uint8_t value)
{
	uint8_t headerByte=0;
	uint16_t txData=0;

	if(regAddress >= 0x0 && regAddress <= 0x2E)
	{
		headerByte = 0; // no read-bit, no burst-bit
		headerByte |= regAddress & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	txData |= value;
	spiWirte(self, txData);
}

uint8_t CC2500_readConfigurationRegister(CC2500_Object* self, CC2500_CONFIGURATION_REGISTER regAddress)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	if(regAddress >= 0x0 && regAddress <= 0x2E)
	{
		headerByte = READ_WRITEn_BIT; // set read-bit, no burst-bit
		headerByte |= regAddress & ADDRESS_MASK;
	}
	txData = headerByte << 8; // shift to high-byte
	rxData = spiWirte(self, txData);
	return (rxData & 0xFF); // mask lower byte
}


// call PIO_InitializeInterrupts(0); before setupIrqHandlers()
void CC2500_setupIrqHandlers(CC2500_Object* self,
		void (*ISR_GD0)(const Pin* pPin),
		void (*ISR_GD2)(const Pin* pPin))
{
	// configure interrupts:
	// PIO_InitializeInterrupts(0);
	PIO_ConfigureIt(&(self->settings.pins.gd0), ISR_GD0);
	PIO_EnableIt(&(self->settings.pins.gd0));
	PIO_ConfigureIt(&(self->settings.pins.gd2), ISR_GD2);
	PIO_EnableIt(&(self->settings.pins.gd2));
}

// read from FIFO is exactly a read status register, but with the address 0x3F
uint8_t CC2500_readFifo(CC2500_Object* self)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	uint8_t regAddress = 0x3F;
	headerByte = READ_WRITEn_BIT | BURST_BIT; // Burst bit has the meaning of "Read Status Register" here
	// the read-bit has the meaning of getting the RX-FIFO remaining bytes
	headerByte |= regAddress & ADDRESS_MASK;

	txData = headerByte << 8; // shift to high-byte
	rxData = spiWirte(self, txData);
	return (rxData & 0xFF); // mask lower byte
}

// write to FIFO is like a write command strobe to address 0x3F, except, that a
// data byte is needed.
void CC2500_writeFifo(CC2500_Object* self, uint8_t data)
{
	uint8_t headerByte=0;
	uint16_t txData=0;

	uint8_t regAddress = 0x3F; // address of FIFO
	headerByte = 0; // no read bit, no burst bit
			// not setting Burst bit has the meaning of "Write Command Strobe" here
			// no read-bit has the meaning of getting the TX-FIFO free bytes available
	headerByte |= regAddress & ADDRESS_MASK;
	txData = headerByte << 8; // shift to high-byte
	txData |= data; // add data to the 16 bit txData
	spiWirte(self, txData);
}

// read from PA-Table is exactly a read status register, but with the address 0x3E
uint8_t CC2500_readPaTable0(CC2500_Object* self)
{
	uint8_t headerByte=0;
	uint16_t txData=0, rxData;

	uint8_t regAddress = 0x3E;
	headerByte = READ_WRITEn_BIT | BURST_BIT; // Burst bit has the meaning of "Read Status Register" here

	headerByte |= regAddress & ADDRESS_MASK;

	txData = headerByte << 8; // shift to high-byte
	rxData = spiWirte(self, txData);
	return (rxData & 0xFF); // mask lower byte
}

// write to PA-Table[0] is like a write command strobe to address 0x3E, except, that a
// data byte is needed.
void CC2500_writePaTable0(CC2500_Object* self, uint8_t data)
{
	uint8_t headerByte=0;
	uint16_t txData=0;

	uint8_t regAddress = 0x3E; // address of PA-Table
	headerByte = 0; // no read bit, no burst bit
			// not setting Burst bit has the meaning of "Write Command Strobe" here
			// no read-bit has the meaning of getting the TX-FIFO free bytes available
	headerByte |= regAddress & ADDRESS_MASK;
	txData = headerByte << 8; // shift to high-byte
	txData |= data; // add data to the 16 bit txData
	spiWirte(self, txData);
}


void CC2500_setSettingsReg(CC2500_Object* self, uint8_t regAddr, uint8_t regValue)
{
	self->settings.configRegs[regAddr] = regValue;
	self->settings.regConfigured[regAddr] = 1;
}


// compare register values in the settings with the registers in the transceiver chip.
// returns -1 on success, else the address of the register with differences
// only configured registers are compared.
int8_t CC2500_compareRegisterValues(CC2500_Object* self)
{
	for(int addr=0; addr < CC2500_NUM_CR; addr++)
	{
		if(self->settings.regConfigured[addr])
		{
			uint8_t chipValue = CC2500_readConfigurationRegister(self, addr);
			if(chipValue != self->settings.configRegs[addr])
			{
				return addr;
			}
		}
	}
	return -1;
}


//----------------------------------------------------------------------------------
// call before initialization of SPI
// 	needs the wait(), therefore it needs a working external time_100us!
//
//  DESCRIPTION:
//    Resets the chip using the procedure described in the datasheet.
//----------------------------------------------------------------------------------
static void resetChip(CC2500_Object* self)
{
	/*
	 *    	HAL_SPI_CS_DEASSERT; --> high
			halMcuWaitUs(30);
			HAL_SPI_CS_ASSERT; --> low
			halMcuWaitUs(30);
			HAL_SPI_CS_DEASSERT; --> high
			halMcuWaitUs(45);

			// Send SRES command
			HAL_SPI_CS_ASSERT; --> low
			while(HAL_SPI_SOMI_VAL); // wait for MISO low
			HAL_SPI_TXBUF_SET(CC2500_SRES); // write Reset Command Strobe
			HAL_SPI_WAIT_TXFIN;

			// Wait for chip to finish internal reset
			while (HAL_SPI_SOMI_VAL); // wait for MISO low again
			HAL_SPI_CS_DEASSERT;
	 */
	// TODO: configure pins for PIO
	Pin sck = self->settings.pins.spiSCK;
	Pin miso = self->settings.pins.spiMISO;
	Pin mosi = self->settings.pins.spiMOSI;
	Pin npcs = self->settings.pins.spiNPCS;

	sck.attribute = PIO_DEFAULT;
	miso.attribute = PIO_PULLUP | PIO_DEGLITCH;
	mosi.attribute = PIO_DEFAULT;
	npcs.attribute = PIO_DEFAULT;

	sck.type = PIO_OUTPUT_1;
	miso.type = PIO_INPUT;
	mosi.type = PIO_OUTPUT_0;
	npcs.type = PIO_OUTPUT_1;

	PIO_Configure(&sck, 1);
	PIO_Configure(&miso, 1);
	PIO_Configure(&mosi, 1);
	PIO_Configure(&npcs, 1);

	// todo: use following 2 lines according to the datasheet, but check SCK for the SRES Cmd
//	PIO_Set(&self->settings.pins.spiSCK);
//	PIO_Clear(&self->settings.pins.spiMOSI);

    // Toggle chip select signal
    PIO_Set(&self->settings.pins.spiNPCS);//HAL_SPI_CS_DEASSERT;
    wait(30);
    PIO_Clear(&self->settings.pins.spiNPCS);// HAL_SPI_CS_ASSERT;
    wait(30);
    PIO_Set(&self->settings.pins.spiNPCS);//HAL_SPI_CS_DEASSERT;
    wait(45);

    // Send SRES command
	PIO_Clear(&self->settings.pins.spiSCK); // SPI Word starts with low level on SCK
    wait(30);
    PIO_Clear(&self->settings.pins.spiNPCS);// HAL_SPI_CS_ASSERT; --> low
    while(PIO_Get(&self->settings.pins.spiMISO)); // while(HAL_SPI_SOMI_VAL);// wait for MISO low

    // clock out SRES by software:
    // The data on the MOSI will be sampled on the rising edge of SCLK
    // -- SCLk is allready high --> first clear SCLK and then set Pin to MSB, before tie SCLK to low!


    uint8_t data = CC2500_CMD_SRES;
    data |= 0; // No Burst bit; R/Wn-Bit doesn't matter here
    uint8_t mask = (1<<7); // MSB first
    while(mask)
    {
    	if(data & mask)
    	{
    		PIO_Set(&self->settings.pins.spiMOSI);
    	}
    	else
    	{
    		PIO_Clear(&self->settings.pins.spiMOSI);
    	}
        wait(30);
        PIO_Set(&self->settings.pins.spiSCK); // write bit to CC2500
        wait(30);
    	PIO_Clear(&self->settings.pins.spiSCK);

    	mask >>= 1;
    }

    // Wait for chip to finish internal reset
    while(PIO_Get(&self->settings.pins.spiMISO)); // wait for MISO-Pin to go low again
    PIO_Set(&self->settings.pins.spiNPCS); //HAL_SPI_CS_DEASSERT;// --> high
}

void wait(uint32_t microSeconds)
{
	uint32_t t0 = time_100us;
	while(time_100us < (t0 + microSeconds/100 + 1)); // wait for PIT to count the time up
}
